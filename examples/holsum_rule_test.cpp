#include "itensor/all.h"
#include"holstein.hpp"
#include "makebasis.hpp"
#include"files.hpp"
#include"timeevclass.hpp"
#include"maketimeevop.hpp"
#include"../src/lboclass.hpp"
#include <boost/program_options.hpp>
using namespace itensor;

int
main(int argc, char *argv[])
    {
     using boost::program_options::value;
  int M{};
  int maxSweeps{};


  int L{};

  double t0{};
  double omega{};
  double gamma{};

  bool saveState{};
  std::string sM{};
std::string mpsName{};
 std::string siteSetName{};
  std::string sMS{};

  std::string sL{};
  std::string st0{};
  std::string somega{};
  std::string sgamma{};

  std::string filename="GSdmrg";

  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("L", boost::program_options::value(&L)->default_value(4), "L")
      ("M", boost::program_options::value(&M)->default_value(4), "M")
      ("MS", boost::program_options::value(&maxSweeps)->default_value(20), "MS")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName");
    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';
      	sL="L"+std::to_string(vm["L"].as<int>());
	filename+=sL;
      }
   
      	     if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
	     if (vm.count("MS"))
      {      std::cout << "Max SWEEPS: " << vm["MS"].as<int>() << '\n';
      	sMS="MS"+std::to_string(vm["MS"].as<int>());
      	filename+=sMS;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      }
	 if (vm.count("gam"))
      {      std::cout << "gamma: " << vm["gam"].as<double>() << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 3);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 3);
      		filename+=somega;
      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
   auto sites2 = readFromFile<Holstein>(siteSetName);
    auto psi0 = readFromFile<MPS>(mpsName);
    double s{0};
       int N=itensor::length(sites2);
       std::vector<double> ndis;
   for(int b = 1; b <= N; b+=2)
        {

   	  psi0.position(b);
  

   			 auto gg2 = AutoMPO(sites2);
  
   		        gg2 += 1,"N",b ;
   		      auto GG2 = toMPO(gg2);
   		      auto en22 = innerC(psi0,GG2,psi0);
   		         printfln("%d %.12f",b,en22);
			 ndis.push_back(real(en22));
   			 s+=real(en22);

   


   }
	     Many_Body::ToFile(ndis, "n_i"+mpsName,ndis.size());
   std::cout<< "s " << s << std::endl;
std::vector<std::complex<double>> kvals1;
   std::vector<std::complex<double>> kvals2;
   for(int k=1; k<=L; k++)
     {
   std::complex<double> s1=0;
   std::complex<double> s2=0;
   double nq=double(k)/(L+1);
  double pi=3.141592653589793;
  std::cout<< "start "<<nq<<std::endl; 
  for(int i=1; i<=L; i+=1)
          {
  //    int i=int(L/2)+1;
   for(int j=1; j<=L; j+=1)
     {
auto  psi6=psi0;
auto  psi3=psi0;

auto  psi4 =psi0;
auto  psi5=psi0;

 int iacc=i*2-1;
 int jacc=j*2-1;
   applyC( psi3, iacc, sites2);
   applyC( psi4, jacc, sites2);
   applyCdag( psi5, jacc, sites2);
   applyCdag( psi6, iacc, sites2);
     // if(i==int(L/2)+1)
     //   {
   s1+=std::sin(nq*pi*i)*std::sin(nq*pi*j)*innerC(psi4,psi3)*2/(L+1);
   s2+=std::sin(nq*pi*i)*std::sin(nq*pi*j)*innerC(psi6,psi5)*2/(L+1);
   // std::cout<<"  " << "j, i "<< j<< " , "<<i << " "<< "overlap cdagc "<< std::sin(nq*pi*i)*std::sin(nq*pi*j)*innerC(psi4,psi3)<<" and "	<< "overlap ccdag "<<  std::sin(nq*pi*i)*std::sin(nq*pi*j)*innerC(psi6,psi5)<<std::endl;
   std::cout<<"  " << "j, i "<< j<< " , "<<i << " "<< "overlap cdagc "<< innerC(psi4,psi3)<<" and "	<< "overlap ccdag "<<  innerC(psi6,psi5)<< "ans " << std::sin(nq*pi*i)*std::sin(nq*pi*j)*innerC(psi4,psi3)*2/(L+1)<< std::endl;
 //}
   
   }
    }
    kvals1.push_back(s1);
    kvals2.push_back(s2);
    std::cout<< "sums "<< s1 << "  " << s2 << std::endl;
     }
             mpsName.replace(mpsName.end()-4, mpsName.end(), ".dat");
	     Many_Body::ToFile(kvals1, "n_k"+mpsName,kvals1.size());
    // std::cout<< "sums "<< (L+1)*s1/(L+1) << "  " << s2<< std::endl;
   std::cout<< "s1 "<< std::accumulate(kvals1.begin(), kvals1.end(), std::complex<double>{0,0})<< "  snd s2 "<< std::accumulate(kvals2.begin(), kvals2.end(), std::complex<double>{0,0})<<std::endl;
	     return 0;
    }
