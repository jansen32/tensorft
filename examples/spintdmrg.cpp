#include"timeevclass.hpp"
#include"maketimeevop.hpp"
#include "itensor/all.h"
using namespace itensor;

using TensorT = IQTensor;
using MPOT = MPOt<TensorT>;
using MPST = MPSt<TensorT>;


int main()
{
  

  auto args = Args("Cutoff=",1E-13,"Maxm=",3000);
  //  auto targs = args;
  double tau=0.0001;
  int N=14;
  auto sites = SpinHalf(2*N);
  double J=1.;
  auto ampo = AutoMPO(sites);
  for(int i=1; i<=2*N-2; i+=2)
    {
        ampo += (0.5*J),"S+",i,"S-",i+2;
        ampo += (0.5*J),"S-",i,"S+",i+2;
        ampo +=        J,"Sz", i,"Sz",i+2;
        }
      auto H = MPOT(ampo);
      auto taua = tau/4.*(1.+1._i);
      auto taub = tau/4.*(1.-1._i);
 println("Making expHa and expHb");
 MPOT expHa,expHb;
 //MPOT expH;
 // auto expH = toExpH<IQTensor>(ampo, -tau);
    expHa = toExpH<TensorT>(ampo,taua);
  expHb = toExpH<TensorT>(ampo,taub);
 // make initial wf
 auto psi = MPST(sites);
    for(int n = 1; n <= 2*N; n += 2)
        {
        auto s1 = sites(n);
        auto s2 = sites(n+1);
        auto wf = TensorT(s1,s2);
        wf.set(s1(1),s2(2), ISqrt2);
        wf.set(s1(2),s2(1), -ISqrt2);
        TensorT D;
        psi.Aref(n) = TensorT(s1);
        psi.Aref(n+1) = TensorT(s2);
        svd(wf,psi.Aref(n),D,psi.Aref(n+1));
        psi.Aref(n) *= D;
        }






//Create a std::vector (dynamically sizeable array)
//to hold the Trotter gates
    using Gate = BondGate<IQTensor>;
 auto gates = std::vector<Gate>();

//Create the gates exp(-i*tstep/2*hterm)
//and add them to gates
 for(int b = 1; b <= 2*(N)-2; b+=2)
    {
    auto hterm = sites.op("Sz",b)*sites.op("Sz",b+1);
    hterm += 0.5*sites.op("S+",b)*sites.op("S-",b+1);
    hterm += 0.5*sites.op("S-",b)*sites.op("S+",b+1);

     
    gates.push_back(Gate(sites,b+1,b+2));
     auto g = Gate(sites,b,b+1,Gate::tImag,tau/4.,hterm);
     gates.push_back(g);
    gates.push_back(Gate(sites,b+1,b+2));
    

    }
//Create the gates exp(-i*tstep/2*hterm) in reverse
//order (to get a second order Trotter breakup which
//does a time step of "tstep") and add them to gates
 for(int b = (2*N)-3; b >= 1; b-=2)
    {
    auto hterm = sites.op("Sz",b)*sites.op("Sz",b+1);
    hterm += 0.5*sites.op("S+",b)*sites.op("S-",b+1);
    hterm += 0.5*sites.op("S-",b)*sites.op("S+",b+1);
    gates.push_back(Gate(sites,b+1,b+2));
        auto g = Gate(sites,b,b+1,Gate::tImag,tau/4.,hterm);
	  gates.push_back(g);
    gates.push_back(Gate(sites,b+1,b+2));
      
    
    }
 spinGates<decltype(sites), std::vector<Gate> > GM(sites, gates, tau, {{"J",1.}});
 int i=0;
 GM.makeGatesFT();
 //  auto args = Args("Cutoff=",cutoff,"Maxm=",5000, "Normalize",true);
  TimeEvolve< IQMPS> TE(gates, psi, args);
    
  //    TE.tDmrgStep();
    auto ttotal = 0.2;
      while(i*tau<=ttotal)
      {
TE.tDmrgStep();
auto en = overlap(psi,H,psi);
printfln("\nEnergy/N %.4f %.20f",ttotal, en/N);
 std::cout<< i*tau<< "  "<< norm(psi)<< "  " << maxM(psi)<< "  "<<std::endl;
  	i+=1;

         }

 // double ttotal=0.2;
 //  auto nt = int(ttotal/tau+(1e-9*(ttotal/tau)));
 // gateTEvol(gates,ttotal,tau,psi,args);
 //      auto en = overlap(psi,H,psi);
 //        printfln("\nEnergy/N %.4f %.20f",ttotal, en/N); 
// for(int n = 1; n <= nt; ++n)
//     {
//       psi = exactApplyMPO(expHa,psi, args);
//       psi = exactApplyMPO(expHb,psi, args);
//     normalize(psi);
//        auto en = overlap(psi,H,psi);
//        printfln("\nEnergy/N %.4f %.20f",n*tau, en/N);
//     }
 
return 0;
}
