#include "itensor/all.h"
#include"holstein.hpp"
#include "makebasis.hpp"
#include"files.hpp"
#include"timeevclass.hpp"
#include"maketimeevop.hpp"
#include"../src/lboclass.hpp"
//#ifdef ITENSOR_USE_TBB
#include "tbb/task_scheduler_init.h"
//#endif
using namespace itensor;

int
main(int argc, char *argv[])
    {
   
  int M{40};
  int maxSweeps{20};
  //#ifdef ITENSOR_USE_TBB
   tbb::task_scheduler_init init(1);
   //#endif

  int L{30};

  double t0{1};
  double omega{1};
  double gamma{std::sqrt(2)};

  bool saveState{0};
  std::string sM{};
  std::string sMS{};

  std::string sL{};
  std::string st0{};
  std::string somega{};
  std::string sgamma{};

  std::string filename="GSdmrg";

 
  filename+=".bin";
      // GS SEARCH
     auto sites2 = Holstein(L,{"ConserveNf=",true,
                             "ConserveNb=",false,
  				"MaxOcc",M});
     std::cout<< "L "<< L << " M "<< M << std::endl;
          auto am2=makeHolstHam(sites2, t0, gamma, omega);
     auto H2 = toMPO(am2);


    auto sweeps = Sweeps(maxSweeps);
    //very important to use noise for this model
  	  sweeps.noise() = 1E-5,1E-5,1E-8, 1E-9,  1E-10, 1E-10, 1E-10;
  	  sweeps.maxdim() = 10,20, 80,200,300, 400, 400, 500, 500;
  	  sweeps.cutoff() = 1E-5, 1E-6, 1E-7, 1E-8, 1E-8, 1E-09, 1E-09, 1E-10, 1E-10;

    auto state2 = InitState(sites2);
    state2.setAll("Emp");
    //         for(int m=1; m<=L; m+=2)
    // {
    // state2.set(m,"Occ");
          state2.set(int(L/2)+1,"Occ");
  	  //  	 }
    auto psi2 = MPS(state2);
    auto [energy,psi0] = itensor::dmrg(H2,psi2,sweeps,{"Quiet=",true});

//  psi0.position(1);
//      psi0.normalize();
//     //  state2 = InitState(sites2);
//     // state2.setAll("Emp");
//     // //     state2.set(1,"Occ");
//     //  state2.set(3,"Occ");
//     //  state2.set(5,"Occ");
//      //     psi0=MPS(state2);
//      // psi0=psi2;
//      // psi0.position(1);
//      // psi0.normalize();
// auto args = itensor::Args("Cutoff=",1e-13,"MaxDim=",3000, "Normalize", true);
//  auto args2 = itensor::Args("Cutoff=",1e-13,"MaxDim=",3000);

//   auto psiOne=applyMPO(H2,psi0, args2);
// //auto H1sqrd=itensor::multSiteOps(H2, H2);
//  auto E0=itensor::innerC(psi0, H2,psi0);
//  std::cout<< "energy "<< E0<<std::endl;
//  auto E0Sqrd=itensor::innerC(psiOne,psiOne);
//  std::cout<< " djei "<< E0Sqrd<<std::endl;
//  	  auto VAL=E0*E0;
//  std::cout<< " djei 2 2 "<< VAL<<std::endl;
//  auto VARIANCE =  std::abs((E0Sqrd-VAL )/E0Sqrd);
//  		    std::cout<< "THE REL GS VAR WAS "<< VARIANCE<<std::endl;
//  		    std::cout<< "THE GS VAR WAS "<< std::abs(E0Sqrd-VAL )<<std::endl;
//      std::cout<< "2el"<<std::endl;
//  AutoMPO Nel(sites2);
//   AutoMPO Nph(sites2);
//       AutoMPO Ek(sites2);
//     AutoMPO x(sites2);
//     for(int b = 1; b <=L ; b+=1)
//       {
// 	std::cout<<"  b "<< b << std::endl;
// Nel += 1,"N",b ;
//   x += 1,"NBdag",b ;
//   x += 1,"NB",b ;
//  Nph += 1,"Nph",b;


//       }
//         for(int b = 1; b <L; b+=1)
// 	  {      

// 	Ek += -t0,"Cdag",b, "C", b+1;
// 	Ek += -t0,"Cdag",b+1, "C", b ;

// }

//      auto NEL = toMPO(Nel);
//      auto NPH = toMPO(Nph);
//      auto X = toMPO(x);
//      auto EK = toMPO(Ek);
//        auto nph=innerC(psi0, NPH, psi0);
//        auto ek=innerC(psi0, EK, psi0);
//        auto smx=innerC(psi0, X, psi0);

// std::cout<< "E_0= "<<E0<<std::endl;
// std::cout<< "NPH_0= "<<nph<<std::endl;

// std::cout<< "E_k= "<<ek<<std::endl;
// std::cout<< "E_{e-ph}= "<<smx<<std::endl;
// 	     if(saveState)
// 		    {

// 		       itensor::writeToFile("MPS"+filename,psi0);
// 		       itensor::writeToFile("siteset"+filename,sites2);
// 		    } 
//     double s{0};
//    for(int b = 1; b <= L; b+=1)
//         {

// 	  psi0.position(b);
  

//    			 auto gg2 = AutoMPO(sites2);
  
//    		        gg2 += 1,"N",b ;
//    		      auto GG2 = toMPO(gg2);
//    		      auto en22 = innerC(psi0,GG2,psi0);
//    		         printfln("%d %.12f",b,en22);
		        
// 			 s+=real(en22);
   


//    }
// std::vector<std::complex<double>> kvals1;
//    std::vector<std::complex<double>> kvals2;
//    for(int k=1; k<=L; k++)
//      {
//    std::complex<double> s1=0;
//    std::complex<double> s2=0;
//    double nq=double(k)/(L+1);
//   double pi=3.141592653589793;
//   std::cout<< "start "<< nq<<std::endl; 
//   for(int i=1; i<=L; i++)
//           {
//   //    int i=int(L/2)+1;
//    for(int j=1; j<=L; j++)
//      {
// auto  psi6=psi0;
// auto  psi3=psi0;

// auto  psi4=psi0;
// auto  psi5=psi0;

// //   std::cout<< "end sum was "<<s<<std::endl;
//    applyC( psi3, i, sites2);
//    applyC( psi4, j, sites2);
//    applyCdag( psi5, j, sites2);
//    applyCdag( psi6, i, sites2);
//      // if(i==int(L/2)+1)
//      //   {
//    s1+=std::sin(nq*pi*i)*std::sin(nq*pi*j)*innerC(psi4,psi3)*2/(L+1);
//    s2+=std::sin(nq*pi*i)*std::sin(nq*pi*j)*innerC(psi6,psi5)*2/(L+1);
//    // std::cout<<"  " << "j, i "<< j<< " , "<<i << " "<< "overlap cdagc "<< std::sin(nq*pi*i)*std::sin(nq*pi*j)*innerC(psi4,psi3)<<" and "	<< "overlap ccdag "<<  std::sin(nq*pi*i)*std::sin(nq*pi*j)*innerC(psi6,psi5)<<std::endl;
//    std::cout<<"  " << "j, i "<< j<< " , "<<i << " "<< "overlap cdagc "<< innerC(psi4,psi3)<<" and "	<< "overlap ccdag "<<  innerC(psi6,psi5) << " abd " << std::sin(nq*pi*i)*std::sin(nq*pi*j)*innerC(psi4,psi3)*2/(L+1)<<std::endl;
//  //}
   
//    }
//     }
//     kvals1.push_back(s1);
//     kvals2.push_back(s2);
//     std::cout<< "sums "<< s1 << "  " << s2 << std::endl;
//      }
//     // std::cout<< "sums "<< (L+1)*s1/(L+1) << "  " << s2<< std::endl;
//    std::cout<< "s1 "<< std::accumulate(kvals1.begin(), kvals1.end(), std::complex<double>{0,0})<< "  snd s2 "<< std::accumulate(kvals2.begin(), kvals2.end(), std::complex<double>{0,0})<<std::endl;
      


// auto J = Index(QN({"T", 1}),3,
// 	       QN({"T",-0}),3,
//                QN({"T",-1}),3,
//                In,"J,Link");
//  auto K = Index(QN({"C", 1}),1,
//                In,"K,Link");
//   auto T = Index(QN({"C", 1}),1,
//                In,"T,Link");



// auto A = ITensor(J,K);
//  auto B = ITensor(dag(J),T);
//  for(int i =1; i<=3; i++)
//    {
//      A.set(J(i),K((1)),1);
//      B.set(dag(J(i)),T((1)),1);
//    }

// print(A);
//  print(B);
//  print(A*B);
	     return 0;
    }
