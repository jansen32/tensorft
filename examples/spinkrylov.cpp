#include"timeevclass.hpp"
#include"maketimeevop.hpp"
#include "itensor/all.h"
#include "krylov.hpp"
using namespace itensor;

using TensorT = IQTensor;
using MPOT = MPOt<TensorT>;
using MPST = MPSt<TensorT>;


int main()
{
  

  auto args = Args("Cutoff=",1E-13,"Maxm=",3000);
  //  auto targs = args;
  double tau=0.001;
  int N=2;
  auto sites = SpinHalf(2*N);
  double J=1.;
  auto ampo = AutoMPO(sites);
  for(int i=1; i<=2*N-2; i+=2)
    {
        ampo += (0.5*J),"S+",i,"S-",i+2;
        ampo += (0.5*J),"S-",i,"S+",i+2;
        ampo +=        J,"Sz", i,"Sz",i+2;
        }
      auto H = MPOT(ampo);
      auto taua = tau/4.*(1.+1._i);
      auto taub = tau/4.*(1.-1._i);
 println("Making expHa and expHb");
 MPOT expHa,expHb;
 //MPOT expH;
 // auto expH = toExpH<IQTensor>(ampo, -tau);
    expHa = toExpH<TensorT>(ampo,taua);
  expHb = toExpH<TensorT>(ampo,taub);
 // make initial wf
 auto psi = MPST(sites);
    for(int n = 1; n <= 2*N; n += 2)
        {
        auto s1 = sites(n);
        auto s2 = sites(n+1);
        auto wf = TensorT(s1,s2);
        wf.set(s1(1),s2(2), ISqrt2);
        wf.set(s1(2),s2(1), -ISqrt2);
        TensorT D;
        psi.Aref(n) = TensorT(s1);
        psi.Aref(n+1) = TensorT(s2);
        svd(wf,psi.Aref(n),D,psi.Aref(n+1));
        psi.Aref(n) *= D;
        }






//Create a std::vector (dynamically sizeable array)
//to hold the Trotter gates
    psi.position(1);
//Create the gates exp(-i*tstep/2*hterm)
//and add them to gates
  int i=0;
    auto ttotal = 0.2;
 
       while(i*tau<=ttotal)
       {
  	 krylov_timeStep(psi, H, -tau*(1.)/2);

 // auto en = overlap(psi,H,psi);
 // printfln("\nEnergy/N %.4f %.20f",ttotal, en/N);
 //  std::cout<< i*tau<< "  "<< norm(psi)<< "  " << maxM(psi)<< "  "<<std::endl;
   	i+=1;

 }

 // double ttotal=0.2;
 //  auto nt = int(ttotal/tau+(1e-9*(ttotal/tau)));
 // gateTEvol(gates,ttotal,tau,psi,args);
 //      auto en = overlap(psi,H,psi);
 //        printfln("\nEnergy/N %.4f %.20f",ttotal, en/N); 
// for(int n = 1; n <= nt; ++n)
//     {
//       psi = exactApplyMPO(expHa,psi, args);
//       psi = exactApplyMPO(expHb,psi, args);
//     normalize(psi);
//        auto en = overlap(psi,H,psi);
//        printfln("\nEnergy/N %.4f %.20f",n*tau, en/N);
//     }
 
return 0;
}
