#include "itensor/all.h"
//#include"holsteinMix.hpp"
#include"holstein.hpp"
#include"makehamiltonians.hpp"
#include"../src/bigSiteAlg.hpp"


using namespace itensor;

int
main()
    {
      auto L=6;
            auto N = L;

     auto t0 = 1.0;
     auto U = 0.0;
     auto ep0 = 0.0;
    // auto K = 0.0;
     std::vector<int> v(N, 2);
     // for(int  i=0; i<N; i++)
     //   {
     // 	  if(i%2==1)v[i]=8;
     //   }
     //      v[1]=8;
      // v[3]=8;
    auto sites = Electron(N);

    // Print(sites);
    auto ampo=AutoMPO(sites);
  for(int i = 1; i <= N; ++i)
    {
    ampo += U,"Nupdn",i;
    }
for(int b = 1; b < N; ++b)
    {
    ampo += -t0,"Cdagup",b,"Cup",b+1;
    ampo += -t0,"Cdagup",b+1,"Cup",b;
    ampo += -t0,"Cdagdn",b,"Cdn",b+1;
    ampo += -t0,"Cdagdn",b+1,"Cdn",b;
    }
auto H = MPO(ampo);




    auto sweeps = Sweeps(100);
    //very important to use noise for this model
    sweeps.noise() = 1E-6,1E-6,1E-8;
    sweeps.maxdim() = 10,20,100,800,800, 800;
    sweeps.cutoff() = 1E-14;

    auto state = InitState(sites);
    for(int i=1; i<=N; i++)
      {
	// if(i%2==0){state.set(i, "+");}
	// else{state.set(i, "-");}
      }
    state.set(2, "+");
      state.set(4, "+");
       state.set(6, "+");
    auto psi0 = MPS(state);
     psi0.position(1);
    psi0.normalize();
       auto KK=sites.op("F", 1);
    print2IndT(KK);
    //      Print(psi0);
       // auto [energy,psi] = dmrg(H,psi0,sweeps,{"Quiet=",true});

       // printfln("Ground State Energy = %.12f",energy);

      // psi0.normalize();
   
      
     //    std::cout<<inner(psi0, Ne, psi0)<<std::endl;
    // auto checkNtot = 0.0;

    // println("Up Electron Density:");
    // for(int j = 1; j <= N; j += 2)
    //     {
    //     psi.position(j);
    //     auto upd = elt(dag(prime(psi(j),"Site"))*op(sites,"Nup",j)*psi(j));
    //     checkNtot += upd;
    //     printfln("%d %.12f",j,upd);
    //     }
    // println();

    // println("Dn Electron Density:");
    // for(int j = 1; j <= N; j += 2)
    //     {
    //     psi.position(j);
    //     auto dnd = elt(dag(prime(psi(j),"Site"))*op(sites,"Ndn",j)*psi(j));
    //     checkNtot += dnd;
    //     printfln("%d %.12f",j,dnd);
    //     }
    // println();

    // Print(checkNtot);

    // println("Boson Density:");
    // for(int j = 2; j <= N; j += 2)
    //     {
    //     psi.position(j);
    //     auto bd= elt(dag(prime(psi(j),"Site"))*op(sites,"N",j)*psi(j));
    //     printfln("%d %.12f",j,bd);
    //     }
    // println();

    return 0;
    }


// 1.28
//Energy/N 0.0100 1.47754105292876669608

//Nph/N 0.1000 1.48418008688648339621
