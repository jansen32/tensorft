#include <iostream>
#include <cmath>
#include "itensor/all.h"
#include"holstein.hpp"
#include "randomState.hpp"
using namespace std;
using namespace itensor;
int main(int argc, char *argv[])
{
    auto L=4;
      auto N = L;
     auto t0 = 1.0;
     auto omega = 1.0;
     auto gamma = 4.0;
     double beta=1;
     double tau=0.01;
     int M=5;
    // auto K = 0.0;
     std::vector<int> v(L, M);
     
  auto sites = Holstein(N,{"ConserveNf=",true,
                             "ConserveNb=",false,
      "DiffMaxOcc=",true, "MaxOccVec=", v});
     auto psi=makeRdState(sites, M);
     std::cout<<"ML "<< maxLinkDim(psi) <<std::endl;
       // 	         for(int b = 1; b <=N; b+=1)
       // {

       // 	     auto bb = AutoMPO(sites);
       // 		      auto gg = AutoMPO(sites);
  
       // 		        gg += 1,"Nph",b ;
       // 		      auto GG = toMPO(gg);
       // 		      auto en2 = inner(psi,GG,psi);
       // 		         printfln("Ph %d %.12f",b,en2);
       // 		      	 auto gg2 = AutoMPO(sites);
  
       // 		        gg2 += 1,"N",b ;
       // 		      auto GG2 = toMPO(gg2);
       // 		      auto en22 = inner(psi,GG2,psi);
       // 		      //	         printfln("El %d %.12f",b,en22);
       // }
    return(0);
}
