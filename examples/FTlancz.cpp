//#include"randomState.hpp"
//#include"timeevclass.hpp"
#include"holstein.hpp"
//#include"trotter.h"
#include "itensor/all.h"
//#include"maketimeevop.hpp"
#include "makebasis.hpp"
#include "FTlancz.hpp"
#include "randomState.hpp"
using namespace itensor;
int main(int argc, char *argv[])
{
      auto L=5;
      int N=L;
auto t0 = 1.0;
     auto omega = 1.0;
     auto gamma = 1.0;
     double beta=10;
     double tau=0.01;
     int rn=5;
     int M=10;
    // auto K = 0.0;
     std::vector<int> v(N, M);
     // v[1]=3;
    auto sites = Holstein(N,{"ConserveNf=",true,
                             "ConserveNb=",false,
	  "DiffMaxOcc=",true, "MaxOccVec=", v});


    Print(sites);
        auto state = InitState(sites);
	
    auto ampo=makeHolstHam(sites, t0, gamma, omega);
    auto H = toMPO(ampo);
    Cplx At{0, 0};
    Cplx Zt{0, 0};
    // for(int i=0; i<rn; i++)
    //   {
    //std::cout<< " rn "<< i << std::endl;
    //     auto psi=makeRdState(sites, M);
    state.set(5,"Occ");
      auto psi=randomMPS(state);
      psi.position(1);
      psi.normalize();
 
    psi.position(1);
    double    nt=0;
        Cplx A{0, 0};
    Cplx Z{0, 0};
    // while(nt<=beta)
    //   {
    	std::cout<< nt<<std::endl;
	krylov_fttimeStep(psi, psi,  H, beta);
	// std::cout<<"ML "<< maxLinkDim(psi) <<std::endl;
 //    nt+=tau;
//       }
//  A+=inner(psi, H, psi);
// Z+=inner(psi,  psi);
//   At+=A;
//   Zt+=Z;
  //}
	//   printfln("<H> = %.12f",At/Zt);
 //     auto sweeps = Sweeps(100);
 //    //very important to use noise for this model
 //    sweeps.noise() = 1E-6,1E-6,1E-8, 1E-10,  1E-12;
 //    sweeps.maxdim() = 10,20,100,100,800;
 //    sweeps.cutoff() = 1E-14;
 // state = InitState(sites);
 //    state.set(5,"Occ");

 //    auto psix = randomMPS(state);
 //     psix.position(1);
 //    psix.normalize();
 //    //      Print(psi0);
 // auto [energy,psic] = dmrg(H,psix,sweeps,{"Quiet=",true});
 // printfln("Ground State Energy = %.12f",energy);
 // 			 int i=	2;
 //     psic.position(i);
 //     psic.normalize();
 //     auto phi=psic.A(i);
 //     //  Print(phi);
 //     auto phidag=dag(prime(phi, "Site"));
 //     auto rho=phi*phidag;
 //     auto tags = TagSet("Site, OM");
 //     int n = i;
 //     //         Print(rho);
 // 		 tags.addTags("n="+str(n));
 // 		 auto [U, D] =diagPosSemiDef(rho,{"ShowEigs", true}); 
  return 0;
}
