//#include"timeevclass.hpp"
#include"holstein.hpp"
//#include"trotter.h"
#include "itensor/all.h"
//#include"maketimeevop.hpp"
#include "makebasis.hpp"
using namespace itensor;
 template<typename T>
void seemat(T Tens)
{
  auto oldInd=Tens.inds()[0];
  auto valInd=Tens.inds()[1];
  auto blocks = doTask(GetBlocks<Real>{Tens.inds(), oldInd, valInd}, Tens.store()); //
  auto Nblock=blocks.size();
  long n=0;
  //      std::cout<< "start " << v <<std::endl;
  for(auto b : range(Nblock))
    {
      
      //std::cout<< v.size()<<std::endl;
      
      auto& B=blocks[b];
      auto M=blocks[b].M;
      // auto M2=blocks2[b].M;
      auto rm=nrows(M);
      auto cm=ncols(M);
      // auto rm2=newInd[b].m();
      //auto cm2=valInd[b].m();
      
      
      for(auto i=0; i<rm; i++)
	{
	  for(auto j=0; j<cm; j++)
	    {
	      //  std::cout<< " b  "<< b<< std::endl;
	      //td::cout<< UU(i, j)<< "change to "<< M(i, j)<<std::endl;
	      std::cout<< M(i, j) << "  ";
	      
	      
	    }
	  std::cout<<std::endl;
	}
    }
}


using LatticeGraph = std::vector<LatticeBond>;
int main()
{
  

  double t0=1;
  double gamma=0.1;
    double omega=1;
    auto args = Args("Cutoff=",1E-13,"Maxm=",3000,"Normalize", true, "t0=", t0, "omega=", omega, "gamma", gamma);
  //  auto targs = args;

  LatticeGraph lattice;
  int L=5;
  int N=2*L;
  int M=3;
  double tau=0.01;    
     auto ttotal = 2.;
   std::vector<int> v(2*N, M);

         v[5]=12;
	 v[7]=12; 
auto sites = Holstein(2*N,{"ConserveNf=",true,
                             "ConserveNb=",false,
      "DiffMaxOcc=",true, "MaxOccVec=", v});
     auto psi=makeBasis(sites, v);
     //     Print(psi);
     
     int i=	2;
     psi.position(i);
     psi.normalize();
     auto phi=psi.A(i);
     //  Print(phi);
     auto phidag=dag(prime(phi, "Site"));
     auto rho=phi*phidag;
     auto tags = TagSet("Site, OM");
     int n = i;
     //         Print(rho);
 		 tags.addTags("n="+str(n));
 		 auto [U, D] =diagPosSemiDef(rho,{"ShowEigs", true}); 
//  auto ampo=makePureHolstHam(sites, t0, gamma, omega);
// std::cout<< " hei "<<std::endl;
//  auto H=toMPO(ampo);
//   std::cout<< " hei "<<std::endl;
//  // Print(sites);2

//   // auto ampo = AutoMPO(sites);

//  //    auto hterm = AutoMPO(sites);
//     auto Nel = AutoMPO(sites);
//     auto Nph = AutoMPO(sites);

//     for(int b = 1; b <=2*N ; b+=4)
//       {
// 	std::cout<<"  b "<< b << std::endl;
// Nel += 1,"N",b ;
//  Nph += 1,"N",b+1;


//       }

//      auto NEL = toMPO(Nel);
//      auto NPH = toMPO(Nph);
     
    




//       psi.position(1);
//       psi.normalize();
//       for(int k=1; k<=2*N; k+=2)
// 	{
// 	  auto c=psi.ref(k);
// 	  auto x = findIndex(c,"Site");
	  
// 	  auto c2=prime(c, x);
// 	  auto cdag=dag(c2);
// 	  auto rho=c*cdag;
// 	  Print(rho);
// 	  seemat(rho);	  
// 	}
//       //     auto sweeps = Sweeps(100);
//     // //very important to use noise for this model
//     // sweeps.noise() = 1E-6,1E-6,1E-8, 1E-10,  1E-12;
//     // sweeps.maxdim() = 10,20,100,100,800;
//     // sweeps.cutoff() = 1E-14;
//     // //  auto [energy,psi0] = dmrg(H,psi,sweeps,{"Quiet=",true});

//     //  printfln("Ground State Energy = %.12f",energy);
//     //  psi0.normalize();

//         	         for(int b = 1; b <= 2*N-1; b+=4)
//        {
// 	 std::cout << "site "<< b << std::endl;
//   	     auto bb = AutoMPO(sites);
  
//   	     bb += 1,"N",b, "A",b+1 ;
//   	     bb += 1,"N",b, "Adag",b+1 ;
//   		      auto BB = toMPO(bb);
//   		      auto en3 = inner(psi,BB,psi);
//         printfln("%d %.12f",b,en3);
//   		      auto gg = AutoMPO(sites);
  
//   		        gg += 1,"N",b+1 ;
//   		      auto GG = toMPO(gg);
//   		      auto en2 = inner(psi,GG,psi);
//   		         printfln("%d %.12f",b,en2);
//  			 auto gg2 = AutoMPO(sites);
  
//   		        gg2 += 1,"N",b ;
//   		      auto GG2 = toMPO(gg2);
//   		      auto en22 =inner(psi,GG2,psi);
//   		         printfln("%d %.12f",b,en22);
		        

   


//   }


//        auto taua = tau/4.*(1.+1._i);
//        auto taub = tau/4.*(1.-1._i);
// //  println("Making expHa and expHb");
//   MPO expHa,expHb;
// //  //MPOT expH;
// //  // auto expH = toExpH<IQTensor>(ampo, -tau);
//      expHa = toExpH(ampo,taua);
//    expHb = toExpH(ampo,taub);


//  auto nt = int(ttotal/tau+(1e-9*(ttotal/tau)));
 

	 
 
// // // 		 // Print(psi);
// // // 	// std::cout<<std::endl;
// // // 	// //
// // //  auto Te=psi.A(5)*psi.A(6);
// // //  auto LL=findtype(psi.A(5), Site); 
// // //  auto m=Te*dag(prime(Te, LL));
// // //  // Print(m);
// // //  //seemat(m);
// // // // std::cout<< overlap(psi, NEL, psi)/N<<std::endl;
// //  normalize(psi);
//  std::cout<< innerC(psi, H, psi)<<std::endl;
//   for(int n = 1; n <= nt; ++n)
//       {
//         psi = applyMPO(expHa,psi, {"Method=","DensityMatrix","Maxm=",1000,"Cutoff=",1E-13});
//   	psi.noPrime();
//         psi = applyMPO(expHb,psi, {"Method=","DensityMatrix","Maxm=",1000,"Cutoff=",1E-13});
//   	psi.noPrime();
//   	psi.normalize();
//          auto en = innerC(psi,H,psi);
//   	       	    printfln("\nEnergy/N %.4f %.20f",n*tau, en/L);
//       }
  // std::cout<< ttotal << std::endl;
//   auto en = overlap(psi, NPH,psi);
//   	    printfln("\nNph/N %.4f %.20f",ttotal, en/N);
// // 	    auto en3 = overlap(psi, HHOP,psi);
// // 	    printfln("\nEnergy/N %.4f %.20f",0.8, en/N);
//   normalize(psi);
//    std::cout<< overlap(psi, H, psi)/((norm(psi)*N*norm(psi)))<<std::endl;
//    std::cout<< overlap(psi, NPH, psi)/((norm(psi)*N*norm(psi)))<<std::endl;
//   	         for(int b = 1; b <= 2*N-1; b+=2)
//        {

//   	     auto bb = AutoMPO(sites);
  
//   	     bb += 1,"N",b, "B",b ;
//   	     bb += 1,"N",b, "Bdag",b ;
//   		      auto BB = IQMPO(bb);
//   		      auto en3 = overlap(psi,BB,psi);
//         printfln("%d %.12f",b,en3);
//   		      auto gg = AutoMPO(sites);
  
//   		        gg += 1,"Nph",b ;
//   		      auto GG = IQMPO(gg);
//   		      auto en2 = overlap(psi,GG,psi);
//   		         printfln("%d %.12f",b,en2);
//  			 auto gg2 = AutoMPO(sites);
  
//   		        gg2 += 1,"N",b ;
//   		      auto GG2 = IQMPO(gg2);
//   		      auto en22 = overlap(psi,GG2,psi);
//   		         printfln("%d %.12f",b,en22);
		        

   


//   }
		 
return 0;
}
//1.24329743531951675273
// 1.24329743531951675273
//Energy/N 0.0100 1.47754105292885817846

//Nph/N 0.0100 1.48418008688657598881
