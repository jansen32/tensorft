#include "itensor/all.h"
#include"holstein.hpp"
#include "makebasis.hpp"
#include"files.hpp"
//#include"timeevclass.hpp"
#include"../src/tdvp.hpp"
#include <boost/program_options.hpp>
#include"../src/tdvp_lbo.hpp"
#include "basisextension.h"
using namespace itensor;
struct dbl_cmp {
    dbl_cmp(double v, double d) : val(v), delta(d) { }
    inline bool operator()(const double &x) const {
        return abs(x-val) < delta;
    }
  private:
    double val, delta;
};
int
main(int argc, char *argv[])
    {
     using boost::program_options::value;
   int M{};
   int Md{};
   int L{};
   double T{};
   double dt{};
   double t0{};
   double omega{};
    int lboMd{};
   double omegap{};
   double gamma{};
   double cutoff{};
 double lbocutoff{};
  std::string sM{};
  std::string sMd{};
  std::string sL{};
  std::string star{};
  std::string sT{};
  std::string st0{};
  std::string somega{};
  std::string somegap{};
  std::string sgamma{};
  std::string starget{};
  std::string scutoff{};
  std::string sPB{};
  std::string sdt;
  std::string stot;
    std::string slboMd{};
    std::string slbocutoff{};
     bool saveAll{};
  std::string filename="FTdisptdvplboKYR";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("L", value(&L)->default_value(4), "L")
      ("T", value(&T)->default_value(0.1), "T")
      ("dt", value(&dt)->default_value(0.01), "dt")
      ("M,m", value(&M)->default_value(2), "M")
      ("Md", value(&Md)->default_value(3000), "Md")
      ("t0", value(&t0)->default_value(1.), "t0")
      ("gam", value(&gamma)->default_value(1.), "gam")
      ("omgp", value(&omegap)->default_value(0.), "omgp")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(100), "lboMd")
       ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-9), "lbocut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-9), "cut")
      ("omg", value(&omega)->default_value(1.), "omg")
      ("SA", boost::program_options::value(&saveAll)->default_value(0), "SA");


      

  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);

  if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';
      	sL="L"+std::to_string(vm["L"].as<int>());
	filename+=sL;
      }
         if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
	filename+=sM;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 5);
      	filename+=st0;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      	somega="omg"+std::to_string(vm["omg"].as<double>()).substr(0, 5);
      		filename+=somega;
      }
		       	 	 if (vm.count("omgp"))
      {      std::cout << "omegap: " << vm["omgp"].as<double>() << '\n';
      	somegap="omgp"+std::to_string(vm["omgp"].as<double>()).substr(0, 5);
      		filename+=somegap;
      }
		 
      		 if (vm.count("gam"))
      {      std::cout << "gamma: " << vm["gam"].as<double>() << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 5);
      		filename+=sgamma;
      }
		 if (vm.count("T"))
      {      std::cout << "T: " << vm["T"].as<double>() << '\n';
      	stot="T"+std::to_string(vm["T"].as<double>()).substr(0, 5);
      		filename+=stot;
      }
		       		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
		       if (vm.count("Md"))
      {      std::cout << "Md: " << vm["Md"].as<int>() << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
	filename+=sMd;
      }
		          {      std::cout << "lboMd: " << vm["lboMd"].as<int>() << '\n';
      	slboMd="lboMd"+std::to_string(vm["lboMd"].as<int>());
      	filename+=slboMd;
      }
			  		 if (vm.count("lbocut"))
		   {
		std::cout << "lbocutoff: " << vm["lbocut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["lbocut"].as<double>();
	 slbocutoff="lbocut"+ss.str();
      	filename+=slbocutoff;
      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  filename+=".bin";
     

  double beta=1./T;
  int N=2*L;
     std::vector<int> v(N, M);

       using Holstein_exp = MixedSiteSet<HolsteinSite_down,HolsteinSite_up>;
        auto sites = Holstein_exp(N,{"ConserveNf=",true,
                              "ConserveNb=",false,
       				"MaxOcc",M});
       auto psi=makeBasis2(sites, M);

       auto ampo=makeHolstHam_dispFT(sites, t0, gamma, omega, omegap);

 auto H=toMPO(ampo);
 
 auto args = Args("Cutoff=",cutoff,"Maxm=",Md, "Normalize",true, "Verbose", false,"MaxDimLBO",lboMd,"MinLBO",1,"CutoffLBO",lbocutoff  ,"SVDMethod", "gesdd");
 auto argsMPO = Args("Cutoff=",cutoff,"Maxm=",Md, "Normalize",false, "Verbose", false,"MaxDimLBO",lboMd,"MinLBO",1,"CutoffLBO",lbocutoff  ,"SVDMethod", "gesdd");

 // std::cout<< "start "<<std::endl;

  std::cout<< "end "<<std::endl;
 AutoMPO Nel(sites);
  AutoMPO Nph(sites);
      AutoMPO Ek(sites);
    AutoMPO x(sites);
   auto curr= makeCurr_FT(sites, t0);
   auto mom1= CurCorr_momentum_left_FT(sites, t0, gamma);
  

    for(int b = 1; b <=N ; b+=2)
      {
	std::cout<<"  b "<< b << std::endl;
Nel += 1,"N",b ;
  x += 1,"NBdag",b ;
  x += 1,"NB",b ;
 Nph += 1,"Nph",b;


      }
        for(int b = 1; b <N-1 ; b+=2)
      {

	Ek += -t0,"Cdag",b, "C", b+2 ;
	Ek += -t0,"Cdag",b+2, "C", b ;

}
   for(int b = 1; b <= N; b+=2)
        {
	  psi.position(b);

   	     auto bb = AutoMPO(sites);
  
   	     bb += 1,"N",b, "B",b ;
   	     bb += 1,"N",b, "Bdag",b ;
   		      auto BB = toMPO(bb);
   		      auto en3 = innerC(psi,BB,psi)/innerC(psi, psi);
        printfln("X %d %.12f",b,en3);
   		      auto gg = AutoMPO(sites);
  
   		        gg += 1,"Nph",b ;
   		      auto GG = toMPO(gg);
   		      auto en2 = innerC(psi,GG,psi)/innerC(psi, psi);
   		         printfln("Nph %d %.12f",b,en2);
   			 auto gg2 = AutoMPO(sites);
  
   		        gg2 += 1,"N",b ;
   		      auto GG2 = toMPO(gg2);
   		      auto en22 = innerC(psi,GG2,psi)/innerC(psi, psi);
   		         printfln("Ne %d %.12f",b,en22);
		        

   
   }
     auto NEL = toMPO(Nel);
     auto NPH = toMPO(Nph);
     auto Curr = toMPO(curr);     
     auto MOM1 = toMPO(mom1);     
     auto X = toMPO(x);
     auto EK = toMPO(Ek);
   auto nt = int(beta/dt+(1e-9*(beta/dt)));


   std::vector<double> Tsstore={0.40, 0.1};
   std::vector<double> nphvec;
   std::vector<double> curvec;
   std::vector<double> curSQvec;
   std::vector<double> evec;
   std::vector<double> xvec;
   std::vector<double> ekvec;
   std::vector<double> tempvec;
    std::vector<double> lboavvec;
    std::vector<double> mxlbo;
    std::vector<double> mxmd;
    std::vector<double> mom1vec;
     using Gate = BondGate;

  
     int Niter=80;  
    auto sweeps = Sweeps(1);
    sweeps.maxdim() = Md;
    sweeps.cutoff() = cutoff;
    sweeps.niter() =Niter;

    auto nph=innerC(psi, NPH, psi)/innerC(psi, psi);
     auto ne=innerC(psi, NEL, psi)/innerC(psi, psi);
 auto ex=innerC(psi, NEL, psi)/innerC(psi, psi);
   
      printfln("\n N/L %.4f %.20f",0, nph/L);
       printfln("\n NE/L %.4f %.20f",0, ne/L);
       printfln("\n E/L %.4f %.20f",0, ex/L);
   double beta2=0;
   TDVP_lbo<MPS> C(psi,H, -dt*0.5, sweeps,args );
   //TDVP<MPS> C(psi,H, -dt*0.5, sweeps,args );
   C.make_lbo();
   std::vector<int> even;
      std::vector<int> odd;
      for(int i=1; i<=N; i+=2)
	{
	  odd.push_back(i);
	}
            for(int i=2; i<=N; i+=2)
	{
	  even.push_back(i);
	}
 int n=0;
        
    	  // Global subspace expansion

    	
    for(int n=1; n<=nt; ++n)
      {
	if(n < 4)
	  {
            // Global subspace expansion
	    std::vector<Real> epsilonK = {1E-12, 1E-12};
            addBasis(psi,H,epsilonK,{"Cutoff",1E-8,
		  "Method","DensityMatrix",
		  "KrylovOrd",3,
		  "DoNormalize",true,
		  "Quiet",true});
	    std::cout<< "Krylov "<<std::endl;
  C.step_new(sweeps); 	  
}
	else{
  C.step(); 
} 
        //   if(dt*n < 0.02)
     	// {
    	//   // Global subspace expansion
    	//   std::vector<Real> epsilonK = {1E-12, 1E-12};
    	//   addBasis(psi,H,epsilonK,{"Cutoff",1E-8,
    	// 	"Method","DensityMatrix",
    	// 	"KrylovOrd",3,
    	// 	"DoNormalize",true,
    	// 	"Quiet",true});
    	// }

	//        start = std::chrono::system_clock::now(); 




  //  end = std::chrono::system_clock::now(); 
  //	 std::chrono::duration<double> elapsed_seconds = end-start;
  //	  std::cout <<  "elapsed time: " << elapsed_seconds.count() << "s\n";
	beta2+=dt;
	
        auto psir2=psi;
       psir2.position(1);
       psir2.normalize();
       auto en=innerC(psir2, H, psir2);
       auto nph=innerC(psir2, NPH, psir2);
       auto ek=innerC(psir2, EK, psir2);
       auto smx=innerC(psir2, X, psir2);
       auto ne=innerC(psir2, NEL, psir2);
       auto curval=innerC(psir2, Curr, psir2);

       	  nphvec.push_back(real(nph));
       	  evec.push_back(real(en));
       	  xvec.push_back(real(smx));
       	  ekvec.push_back(real(ek));
	  curvec.push_back(real(curval));
       auto y1 = applyMPO(Curr,psir2,argsMPO);
         y1.noPrime();    
 auto curSQval=innerC(y1, y1);
 auto mom1val=innerC(psir2,MOM1, y1);
	 curSQvec.push_back(real(curSQval));
mom1vec.push_back(real(mom1val));
     	  tempvec.push_back(1./beta2);
	       	  mxlbo.push_back(static_cast<double>(C.maxLboDim()));
	  mxmd.push_back(static_cast<double>(maxLinkDim(psir2)));
	  //     	  lboavvec.push_back(C.avLboDim());

	  printfln("\n at T/ omega Energy/L %.4f %.20f",1./(beta2*omega), en/L);
             printfln("\n wt beta omega Nph/L %.4f %.20f", n*dt*omega, nph/L);
	     	     std::cout<< "com "<< real(en) << "  "<< real(ek)+real(smx)*gamma+omega*real(nph) << "  lbo mx ogg "<<C.maxLboDim_at(odd)<<"  lbo mx enen "<<C.maxLboDim_at(even)<<" max BD "<< maxLinkDim(psir2)  << "  electron number "<< ne<<std::endl;
		     //std::cout<< "com "<< real(en) << "  "<< real(ek)+real(smx)*gamma+omega*real(nph) << "  lbo mx ogg "<<"  lbo mx enen "<<" max BD "<< maxLinkDim(psir2)  << "  electron number "<< ne<<std::endl;
    
	     if(saveAll && (std::find_if(Tsstore.begin(), Tsstore.end(), dbl_cmp((1./beta2), 1E-8))!=Tsstore.end()))
		    {

 std::string SC="aT"+std::to_string(1./beta2).substr(0, 4);
//		      Print(psir2);
		      itensor::writeToFile("MPS"+SC+filename,psir2);
		      itensor::writeToFile("siteset"+SC+filename,sites);
		      auto psi2=psi;
		     double s{0};
     double s2{0};
   for(int b = 1; b <= N; b+=1)
        {
	  psi2.position(b);
   	     auto bb = AutoMPO(sites);
  
   	     bb += 1,"N",b, "B",b ;
   	     bb += 1,"N",b, "Bdag",b ;
   		      auto BB = toMPO(bb);
   		      auto en3 = innerC(psi2,BB,psi2)/innerC(psi2, psi2);
        printfln("X %d %.12f",b,en3);
   		      auto gg = AutoMPO(sites);
  
   		        gg += 1,"Nph",b ;
   		      auto GG = toMPO(gg);
   		      auto en2 = innerC(psi2,GG,psi2)/innerC(psi2, psi2);
   		         printfln("Nph %d %.12f",b,en2);
   			 auto gg2 = AutoMPO(sites);
  
   		        gg2 += 1,"N",b ;
   		      auto GG2 = toMPO(gg2);
   		      auto en22 = innerC(psi2,GG2,psi2)/innerC(psi2, psi2);
   		         printfln("Ne %d %.12f",b,en22);
			 if((b+1)%2==0)
			   {
			 s+=real(en22);
			   }
			 else{
			   s2+=real(en22);
			 }

	}
		    }
	 
     }

  // //
    auto psi2=psi;

	 // std::cout<< " f1 "<< innerC(psi1,Ne, psi2)<<'\n';
    double s{0};
     double s2{0};
   for(int b = 1; b <= N; b+=1)
        {
	  psi2.position(b);
   	     auto bb = AutoMPO(sites);
  
   	     bb += 1,"N",b, "B",b ;
   	     bb += 1,"N",b, "Bdag",b ;
   		      auto BB = toMPO(bb);
   		      auto en3 = innerC(psi2,BB,psi2)/innerC(psi2, psi2);
        printfln("X %d %.12f",b,en3);
   		      auto gg = AutoMPO(sites);
  
   		        gg += 1,"Nph",b ;
   		      auto GG = toMPO(gg);
   		      auto en2 = innerC(psi2,GG,psi2)/innerC(psi2, psi2);
   		         printfln("Nph %d %.12f",b,en2);
   			 auto gg2 = AutoMPO(sites);
  
   		        gg2 += 1,"N",b ;
   		      auto GG2 = toMPO(gg2);
   		      auto en22 = innerC(psi2,GG2,psi2)/innerC(psi2, psi2);
   		         printfln("Ne %d %.12f",b,en22);
			 if((b+1)%2==0)
			   {
			 s+=real(en22);
			   }
			 else{
			   s2+=real(en22);
			 }
   


   }
   std::cout<< "end sum was in phys "<<s<<std::endl;
   std::cout<< "end sum was in aux "<<s2<<std::endl;
  		 Many_Body::bin_write("temp"+filename, tempvec);
  		 Many_Body::bin_write("nX"+filename, xvec);
		 Many_Body::bin_write("J"+filename, curvec);
		 Many_Body::bin_write("JJ"+filename, curSQvec);
		 Many_Body::bin_write("mom1"+filename, mom1vec);
  		 Many_Body::bin_write("EK"+filename, ekvec);
  		 Many_Body::bin_write("Nph"+filename, nphvec);
  		 Many_Body::bin_write("E"+filename, evec);
		 Many_Body::bin_write("mxLBO"+filename, mxlbo);
		 //	 Many_Body::bin_write("avLBO"+filename, lboavvec);
		  Many_Body::bin_write("mxBD"+filename, mxmd);
		
		 
    return 0;
    
    }

