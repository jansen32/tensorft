#include "itensor/all.h"
#include"holstein.hpp"
#include "makebasis.hpp"
#include"files.hpp"
#include"timeevclass.hpp"
#include"maketimeevop.hpp"
#include <boost/program_options.hpp>
using namespace itensor;

int
main(int argc, char *argv[])
    {
     using boost::program_options::value;
   int M{};
   int Md{};
   int L{};
   double T{};
   double dt{};
   double t0{};
   double omega{};
   double gamma{};
   double cutoff{};

  std::string sM{};
  std::string sMd{};
  std::string sL{};
  std::string star{};
  std::string sT{};
  std::string st0{};
  std::string somega{};
  std::string sgamma{};
  std::string starget{};
  std::string scutoff{};
  std::string sPB{};
  std::string sdt;
  std::string stot;
  std::string filename="fttebd";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("L", value(&L)->default_value(4), "L")
      ("T", value(&T)->default_value(0.1), "T")
      ("dt", value(&dt)->default_value(0.01), "dt")
      ("M,m", value(&M)->default_value(2), "M")
      ("Md", value(&Md)->default_value(3000), "Md")
      ("t0", value(&t0)->default_value(1.), "t0")
      ("gam", value(&gamma)->default_value(1.), "gam")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-9), "cut")
      ("omg", value(&omega)->default_value(1.), "omg");


      

  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);

  if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';
      	sL="L"+std::to_string(vm["L"].as<int>());
	filename+=sL;
      }
         if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
	filename+=sM;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      	somega="omg"+std::to_string(vm["omg"].as<double>()).substr(0, 3);
      		filename+=somega;
      }
		 
      		 if (vm.count("gam"))
      {      std::cout << "gamma: " << vm["gam"].as<double>() << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 3);
      		filename+=sgamma;
      }
		 if (vm.count("T"))
      {      std::cout << "T: " << vm["T"].as<double>() << '\n';
      	stot="T"+std::to_string(vm["T"].as<double>()).substr(0, 3);
      		filename+=stot;
      }
		       		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
		       if (vm.count("Md"))
      {      std::cout << "Md: " << vm["Md"].as<int>() << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
	filename+=sMd;
      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  filename+=".bin";
      // GS SEARCH
     auto sites2 = Holstein(L,{"ConserveNf=",true,
                             "ConserveNb=",false,
				"MaxOcc",M});
          auto am2=makeHolstHam(sites2, t0, gamma, omega);
     auto H2 = toMPO(am2);


    auto sweeps = Sweeps(100);
    //very important to use noise for this model
    sweeps.noise() = 1E-6,1E-6,1E-8, 1E-10,  1E-12;
    sweeps.maxdim() = 10,20,100,800,800;
    sweeps.cutoff() = 1E-14;

    auto state2 = InitState(sites2);
    state2.set(1,"Occ");
     // state.set(3,"Occ");
     // state.set(5,"Occ");
    auto psi02 = MPS(state2);
     psi02.position(1);
    psi02.normalize();
    // auto KK=sites.op("F", 1);
    // print2IndT(KK);
    //      Print(psi0);
    //auto [energy2,psi2] = dmrg(H2,psi02,sweeps,{"Quiet=",true});
       // END GS SEARCH
  double beta=1./T;
  int N=2*L;
     std::vector<int> v(N, M);
     // for(int  i=0; i<N; i++)
     //   {
     // 	  if(i%2==1)v[i]=8;
     //   }
     //    v[1]=8;
      // v[3]=8;
  // auto sites = Holstein(N,{"ConserveNf=",true,
  //                            "ConserveNb=",false,
  //     "DiffMaxOcc=",true, "MaxOccVec=", v});
       auto sites = Holstein(N,{"ConserveNf=",true,
                             "ConserveNb=",false,
				"MaxOcc",M});
   auto psi=makeBasis(sites, M);

 auto ampo=makePureHolstHam(sites, t0, gamma, omega);

 auto H=toMPO(ampo);
 AutoMPO Nel(sites);
  AutoMPO Nph(sites);
      AutoMPO Ek(sites);
    AutoMPO x(sites);
    for(int b = 1; b <=N ; b+=2)
      {
	std::cout<<"  b "<< b << std::endl;
Nel += 1,"N",b ;
  x += 1,"NBdag",b ;
  x += 1,"NB",b ;
 Nph += 1,"Nph",b;


      }
        for(int b = 1; b <N-1 ; b+=2)
      {

	Ek += -t0,"Cdag",b, "C", b+2 ;
	Ek += -t0,"Cdag",b+2, "C", b ;

}

     auto NEL = toMPO(Nel);
     auto NPH = toMPO(Nph);
     auto X = toMPO(x);
     auto EK = toMPO(Ek);
   auto nt = int(beta/dt+(1e-9*(beta/dt)));


   std::vector<double> nphvec;
   std::vector<double> evec;
   std::vector<double> xvec;
   std::vector<double> ekvec;
   std::vector<double> tempvec;
     using Gate = BondGate;

    auto args = Args("Cutoff=",cutoff,"Maxm=",Md, "DoNormalize",true);
 auto gates = vector<Gate>();
    std::map<std::string, double> param;
 param.insert({"t0",t0});
 param.insert({"omega",omega});
 param.insert({"gamma",gamma});
 param.insert({"C",0});
    IHG GM(sites, gates, 0.5*dt, param);
    GM.makeFTGates();
       auto psiv=vMPS(psi);
       TimeEvolve< vMPS, true> TE(gates, psiv, L, args);
	    //TimeEvolve< MPS> TE(gates, psi, L, args);
      //  psi.normalize();
    auto nph=innerC(psi, NPH, psi)/innerC(psi, psi);
     auto ne=innerC(psi, NEL, psi)/innerC(psi, psi);
 auto ex=innerC(psi, NEL, psi)/innerC(psi, psi);
      printfln("\n N/L %.4f %.20f",0, nph/L);
       printfln("\n NE/L %.4f %.20f",0, ne/L);
       printfln("\n E/L %.4f %.20f",0, ex/L);
   double beta2=0;
  
   std::cout<< "HERER "<< std::endl;
   for(int n=1; n<=nt; ++n)
     {
       
        TE.TEBDStepO2();
       // TE.tDmrgStep();
       
       //                     TE.TEBDStepO2para();
       // psi.normalize();
      	beta2+=dt;
       tempvec.push_back(1./beta2);
       
       auto en=innerC(psi, H, psi)/innerC(psi, psi);
       auto nph=innerC(psi, NPH, psi)/innerC(psi, psi);
       auto ek=innerC(psi, EK, psi)/innerC(psi, psi);
       auto smx=innerC(psi, X, psi)/innerC(psi, psi);
       	  nphvec.push_back(real(nph));
       	  evec.push_back(real(en));
      	  xvec.push_back(real(smx));
      	  ekvec.push_back(real(ek));
       	  std::cout<<"max BD "<< maxLinkDim(psi)<<std::endl;
       printfln("\n Energy/L %.4f %.20f",1./beta2, en);
             printfln("\n Nph/L %.4f %.20f", n*dt, nph/L);
      	          std::cout<< "com "<< real(en) << "  "<< real(ek)+real(smx)*gamma+real(nph)<<std::endl;
    
       
	 
     }
     	         for(int b = 1; b <= N; b+=2)
       {

  	     auto bb = AutoMPO(sites);
  
  	     bb += 1,"N",b, "B",b ;
  	     bb += 1,"N",b, "Bdag",b ;
  		      auto BB = toMPO(bb);
  		      auto en3 = innerC(psi,BB,psi)/innerC(psi, psi);
        printfln("%d %.12f",b,en3);
  		      auto gg = AutoMPO(sites);
  
  		        gg += 1,"Nph",b ;
  		      auto GG = toMPO(gg);
  		      auto en2 = innerC(psi,GG,psi)/innerC(psi, psi);
  		         printfln("%d %.12f",b,en2);
 			 auto gg2 = AutoMPO(sites);
  
  		        gg2 += 1,"N",b ;
  		      auto GG2 = toMPO(gg2);
  		      auto en22 = innerC(psi,GG2,psi)/innerC(psi, psi);
  		         printfln("%d %.12f",b,en22);
		        

   


  }
		 
		 Many_Body::bin_write("temp"+filename, tempvec);
		 Many_Body::bin_write("nX"+filename, xvec);
		 Many_Body::bin_write("EK"+filename, ekvec);
		 Many_Body::bin_write("Nph"+filename, nphvec);
		 Many_Body::bin_write("E"+filename, evec);
		 
    return 0;
    
    }

