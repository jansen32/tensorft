#include "itensor/all.h"
#include"holstein.hpp"

#include"files.hpp"


#include"include/dmrg_lbo.h"
#include <boost/program_options.hpp>
using namespace itensor;

int
main(int argc, char *argv[])
    {
     using boost::program_options::value;
  int M{};
  int maxSweeps{};


  int L{};

  double t0{};
  double omega{};
  double gamma{};

  bool saveState{};
  std::string sM{};
  std::string sMS{};

  std::string sL{};
  std::string st0{};
  std::string somega{};
  std::string sgamma{};

  std::string filename="GSdmrg";

  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("L", boost::program_options::value(&L)->default_value(4), "L")
      ("M", boost::program_options::value(&M)->default_value(4), "M")
      ("MS", boost::program_options::value(&maxSweeps)->default_value(20), "MS")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("SS", boost::program_options::value(&saveState)->default_value(false), "SS");
    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';
      	sL="L"+std::to_string(vm["L"].as<int>());
	filename+=sL;
      }
   
      	     if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
	     if (vm.count("MS"))
      {      std::cout << "Max SWEEPS: " << vm["MS"].as<int>() << '\n';
      	sMS="MS"+std::to_string(vm["MS"].as<int>());
      	filename+=sMS;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      }
	 if (vm.count("gam"))
      {      std::cout << "gamma: " << vm["gam"].as<double>() << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 3);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 3);
      		filename+=somega;
      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  filename+=".bin";
      // GS SEARCH
     auto sites2 = Holstein(L,{"ConserveNf=",true,
                             "ConserveNb=",false,
				"MaxOcc",M});
     std::cout<< "L "<< L << " M "<< M << std::endl;
          auto am2=makeHolstHam(sites2, t0, gamma, omega);
     auto H2 = toMPO(am2);


       auto sweeps = Sweeps(maxSweeps);
    
    // noise terms are very important to get the correct results
       sweeps.noise() = 1E-6,1E-6,1E-6, 1E-6,  1E-12;
       sweeps.maxdim() = 10,20,100,100,800,800;
       sweeps.cutoff() = 1E-7;
    auto state2 = InitState(sites2);
    state2.setAll("Emp");
    //         for(int m=1; m<=L; m+=2)
    // {
    // state2.set(m,"Occ");
    //     state2.set(5,"Occ");
	  state2.set(1,"Occ");
	  // state2.set(2,"Occ");
	  	   // state2.set(3,"Occ");
		   //   state2.set(7,"Occ");
		   //     state2.set(9,"Occ");
	  //  	 }
	  auto args = itensor::Args("Cutoff=",1e-13,"MaxDim=",3000, "Normalize", true);
	  auto args2 = itensor::Args("Cutoff=",0,"MinDim=", 10,"MaxDim=",3000,"CutoffLBO=",1e-12, "MinDimLBO=",(12),"Quiet=",true);
	  //"ShowEigs", true,
 
    auto psi2 = MPS(state2);
    auto [energy,psi0] = dmrg_lbo(H2,psi2,sweeps,args2);

 auto E0=itensor::innerC(psi0, H2,psi0)/itensor::innerC(psi0, psi0);
 std::cout<< "energy "<< E0<<std::endl;
 psi0.position(1);
     psi0.normalize();
    //  state2 = InitState(sites2);
    // state2.setAll("Emp");
    // //     state2.set(1,"Occ");
    //  state2.set(3,"Occ");
    //  state2.set(5,"Occ");
     //     psi0=MPS(state2);
     // psi0=psi2;
     // psi0.position(1);
     // psi0.normalize();
auto psiOne=applyMPO(H2,psi0, args2);
//auto H1sqrd=itensor::multSiteOps(H2, H2);

 auto E0Sqrd=itensor::innerC(psiOne,psiOne);
 std::cout<< " djei "<< E0Sqrd<<std::endl;
 	  auto VAL=E0*E0;
 std::cout<< " djei 2 2 "<< VAL<<std::endl;
 auto VARIANCE =  std::abs((E0Sqrd-VAL )/E0Sqrd);
 		    std::cout<< "THE REL GS VAR WAS "<< VARIANCE<<std::endl;
 		    std::cout<< "THE GS VAR WAS "<< std::abs(E0Sqrd-VAL )<<std::endl;
     std::cout<< "2el"<<std::endl;
 AutoMPO Nel(sites2);
  AutoMPO Nph(sites2);
      AutoMPO Ek(sites2);
    AutoMPO x(sites2);
    for(int b = 1; b <=L ; b+=1)
      {
	std::cout<<"  b "<< b << std::endl;
Nel += 1,"N",b ;
  x += 1,"NBdag",b ;
  x += 1,"NB",b ;
 Nph += 1,"Nph",b;


      }
        for(int b = 1; b <L; b+=1)
	  {      

	Ek += -t0,"Cdag",b, "C", b+1;
	Ek += -t0,"Cdag",b+1, "C", b ;

}

     auto NEL = toMPO(Nel);
     auto NPH = toMPO(Nph);
     auto X = toMPO(x);
     auto EK = toMPO(Ek);
       auto nph=innerC(psi0, NPH, psi0);
       auto ek=innerC(psi0, EK, psi0);
       auto smx=innerC(psi0, X, psi0);
         auto nel=innerC(psi0, Nel, psi0);

 std::cout<< "E_0= "<<E0<<std::endl;
        std::cout<< "N_e= "<<nel<<std::endl;
        std::cout<< "N_ph= "<<nph<<std::endl;
// std::cout<< "NPH_0= "<<nph<<std::endl;

// std::cout<< "E_k= "<<ek<<std::endl;
// std::cout<< "E_{e-ph}= "<<smx<<std::endl;
// 	     if(saveState)
// 		    {

// 		       itensor::writeToFile("MPS"+filename,psi0);
// 		       itensor::writeToFile("siteset"+filename,sites2);
// 		    } 
//     double s{0};
//    for(int b = 1; b <= L; b+=1)
//         {

// 	  psi0.position(b);
  

//    			 auto gg2 = AutoMPO(sites2);
  
//    		        gg2 += 1,"N",b ;
//    		      auto GG2 = toMPO(gg2);
//    		      auto en22 = innerC(psi0,GG2,psi0);
//    		         printfln("%d %.12f",b,en22);
		        
// 			 s+=real(en22);
   


//    }
// std::vector<std::complex<double>> kvals1;
//    std::vector<std::complex<double>> kvals2;
//    for(int k=1; k<=L; k++)
//      {
//    std::complex<double> s1=0;
//    std::complex<double> s2=0;
//    double nq=double(k)/(L+1);
//   double pi=3.141592653589793;
//   std::cout<< "start "<< nq<<std::endl; 
//   for(int i=1; i<=L; i++)
//           {
//   //    int i=int(L/2)+1;
//    for(int j=1; j<=L; j++)
//      {
// auto  psi6=psi0;
// auto  psi3=psi0;

// auto  psi4=psi0;
// auto  psi5=psi0;

// //   std::cout<< "end sum was "<<s<<std::endl;
//    applyC( psi3, i, sites2);
//    applyC( psi4, j, sites2);
//    applyCdag( psi5, j, sites2);
//    applyCdag( psi6, i, sites2);
//      // if(i==int(L/2)+1)
//      //   {
//    s1+=std::sin(nq*pi*i)*std::sin(nq*pi*j)*innerC(psi4,psi3)*2/(L+1);
//    s2+=std::sin(nq*pi*i)*std::sin(nq*pi*j)*innerC(psi6,psi5)*2/(L+1);
//    // std::cout<<"  " << "j, i "<< j<< " , "<<i << " "<< "overlap cdagc "<< std::sin(nq*pi*i)*std::sin(nq*pi*j)*innerC(psi4,psi3)<<" and "	<< "overlap ccdag "<<  std::sin(nq*pi*i)*std::sin(nq*pi*j)*innerC(psi6,psi5)<<std::endl;
//    std::cout<<"  " << "j, i "<< j<< " , "<<i << " "<< "overlap cdagc "<< innerC(psi4,psi3)<<" and "	<< "overlap ccdag "<<  innerC(psi6,psi5) << " abd " << std::sin(nq*pi*i)*std::sin(nq*pi*j)*innerC(psi4,psi3)*2/(L+1)<<std::endl;
//  //}
   
//    }
//     }
//     kvals1.push_back(s1);
//     kvals2.push_back(s2);
//     std::cout<< "sums "<< s1 << "  " << s2 << std::endl;
//      }
//     // std::cout<< "sums "<< (L+1)*s1/(L+1) << "  " << s2<< std::endl;
//    std::cout<< "s1 "<< std::accumulate(kvals1.begin(), kvals1.end(), std::complex<double>{0,0})<< "  snd s2 "<< std::accumulate(kvals2.begin(), kvals2.end(), std::complex<double>{0,0})<<std::endl;
	     return 0;
    }
