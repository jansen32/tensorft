#include"timeevclass.hpp"
#include"holstein.hpp"
#include"trotter.h"
#include "itensor/all.h"
#include"maketimeevop.hpp"
#include "makebasis.hpp"
using namespace itensor;

using TensorT = IQTensor;
using MPOT = MPOt<TensorT>;
using MPST = MPSt<TensorT>;
template<typename T>
void seemat(T Tens)
{
  auto oldInd=Tens.inds()[0];
  auto valInd=Tens.inds()[1];
  auto blocks = doTask(GetBlocks<Real>{Tens.inds(), oldInd, valInd}, Tens.store()); //
  auto Nblock=blocks.size();
  long n=0;
  //      std::cout<< "start " << v <<std::endl;
  for(auto b : range(Nblock))
    {
      
      //std::cout<< v.size()<<std::endl;
      
      auto& B=blocks[b];
      auto M=blocks[b].M;
      // auto M2=blocks2[b].M;
      auto rm=nrows(M);
      auto cm=ncols(M);
      // auto rm2=newInd[b].m();
      //auto cm2=valInd[b].m();
      
      
      for(auto i=0; i<rm; i++)
	{
	  for(auto j=0; j<cm; j++)
	    {
	      //  std::cout<< " b  "<< b<< std::endl;
	      //td::cout<< UU(i, j)<< "change to "<< M(i, j)<<std::endl;
	      std::cout<< M(i, j) << "  ";
	      
	      
	    }
	  std::cout<<std::endl;
	}
    }
}
using LatticeGraph = std::vector<LatticeBond>;
int main()
{
  

  double t0=1;
  double gamma=1;
    double omega=1;
    auto args = Args("Cutoff=",1E-13,"Maxm=",3000,"Normalize", true, "t0=", t0, "omega=", omega, "gamma", gamma);
  //  auto targs = args;
  double tau=0.00001;
  LatticeGraph lattice;
  
  int N=4;
  int M=4;
    

  auto sites = Holstein(2*N, M);


  

  auto ampo = AutoMPO(sites);

   auto hterm = AutoMPO(sites);
   auto Nel = AutoMPO(sites);
   auto Nph = AutoMPO(sites);

    for(int b = 1; b <= 2*N-2; b+=2)
      {
      	hterm += -t0,"Cdag",b+2,"C",b;
      	hterm += -t0, "Cdag",b,"C",b+2 ;
Nel += 1,"Cdag",b,"C",b ;
 Nph += 1,"Nph",b;
 hterm += gamma,"NBdag",b;
 hterm += gamma,"NB",b;
      	// hterm += 0.5*gamma,"Id", b,"NB",b+1;
      	// hterm += 0.5*gamma,"Id", b,"NBdag",b+1;
 hterm += omega,"Nph",b;
 //V.push_bacl

      }
    hterm+=omega,"Nph", 2*N-1;
    hterm += gamma,"NB",2*N-1;
    hterm += gamma, "NBdag",2*N-1;
	 
    Nel += 1,"Cdag",2*N-1,"C",2*N-1 ;
    Nph += 1,"Nph",2*N-1 ;

     auto H = IQMPO(hterm);
     auto NEL = IQMPO(Nel);
     auto NPH = IQMPO(Nph);
     
    


//  auto state = InitState(sites);
//   auto state4 = InitState(sites);
//  state.setAll("Emp");
//   state4.setAll("Emp");
 
//   state.set(1, "Occ");
//    state.set(2, "Occ");

    
// auto psi = IQMPS(state);

//        for(int b = 3; b <= 2*N-1; b+=2)
//        {
// 	 auto state2 = InitState(sites);
// 	 state2.setAll("Emp");
// 	 state2.set(b, "Occ");
// 	 state2.set(b+1, "Occ");
// 	 auto psi2=IQMPS(state2);
// 	 psi= sum(psi, psi2);
//    }

//           for(int b = 1; b <= 2*N-1; b+=2)
//        {
// 		 auto c = AutoMPO(sites);
// 		 c += 1./std::sqrt(2*3),"Bdag",b,"Bdag",b ;
//      c += 1./std::sqrt(3),"Bdag",b,"Bdag",b+1 ;
//      c += 1./std::sqrt(3*2),"Bdag",b+1,"Bdag",b+1 ;

//       auto C = IQMPO(c);

//       psi=applyMPO(C, psi);

//    }
     auto psi=makeBasis(sites, M);
      auto taua = tau/4.*(1.+1._i);
      auto taub = tau/4.*(1.-1._i);
 println("Making expHa and expHb");
 MPOT expHa,expHb;
 //MPOT expH;
 // auto expH = toExpH<IQTensor>(ampo, -tau);
    expHa = toExpH<TensorT>(hterm,taua);
  expHb = toExpH<TensorT>(hterm,taub);
    auto ttotal = 0.1;
    using Gate = BondGate<IQTensor>;
 auto gates = std::vector<Gate>();
 holstGates<decltype(sites), std::vector<Gate> > GM(sites, gates, tau, {{"t0",t0}, {"gamma", gamma}, {"omega", omega}});
 std::cout<< "start "<< std::endl;
 GM.makeGatesFT();
 normalize(psi);
 seemat(psi.A(1));
 for(int i=1; i<=2*N; i+=2)
   {
     std::cout << " for "<< i << std::endl;
 auto A1=psi.A(i)*psi.A(i+1);
 auto A2=(A1);
 auto l=A1.inds();
 Print(l[0]);
 A2.prime(l[0]);
 Print(A2);
 auto rho=A1*dag(A2);
 std::cout<<std::endl;
 Print(rho);
  seemat(rho);
   }
   Args targs;
    targs.add("Verbose",false);
    targs.add("Maxm",2000);
    targs.add("Minm",6);
    targs.add("Cutoff",1E-13);
     targs.add("Normalize",false);
    //    TimeEvolve< IQMPS> TE(gates, psi, targs);
    TimeEvolve< IQMPS> TE(gates, psi, targs);
    
  //    TE.tDmrgStep();
  int i=0;
//   normalize(psi);
//       // auto en2 = overlap(psi,NPH,psi);
//       // 	 printfln("\nEnergy/N %.4f %.20f",0, en2/N);
//        while(i*tau<=ttotal)
//        {
//   TE.tDmrgStep();
//   normalize(psi);
//  auto en = overlap(psi,H,psi);
//  printfln("\nEnergy/N %.4f %.20f",ttotal, en/(norm(psi)*N*norm(psi)));
 
// std::cout<< i*tau<< "  "<< norm(psi)<< "  " << maxM(psi)<< "  "<<std::endl;
//    	i+=1;

//        }

auto nt = int(ttotal/tau+(1e-9*(ttotal/tau)));
 

	 
 
// 		 // Print(psi);
// 	// std::cout<<std::endl;
// 	// //
//  auto Te=psi.A(5)*psi.A(6);
//  auto LL=findtype(psi.A(5), Site); 
//  auto m=Te*dag(prime(Te, LL));
//  // Print(m);
//  //seemat(m);
// // std::cout<< overlap(psi, NEL, psi)/N<<std::endl;
 normalize(psi);
 for(int n = 1; n <= nt; ++n)
     {
       psi = applyMPO(expHa,psi, {"Method=","DensityMatrix","Maxm=",1000,"Cutoff=",1E-13});
       psi = applyMPO(expHb,psi, {"Method=","DensityMatrix","Maxm=",1000,"Cutoff=",1E-13});
     normalize(psi);
        auto en = overlap(psi,H,psi);
     	    printfln("\nEnergy/N %.4f %.20f",n*tau, en/N);
     }
  auto en = overlap(psi, NPH,psi);
  	    printfln("\nNph/N %.4f %.20f",ttotal, en/N);
// 	    auto en3 = overlap(psi, HHOP,psi);
// 	    printfln("\nEnergy/N %.4f %.20f",0.8, en/N);
  normalize(psi);
   std::cout<< overlap(psi, H, psi)/((norm(psi)*N*norm(psi)))<<std::endl;
   std::cout<< overlap(psi, NPH, psi)/((norm(psi)*N*norm(psi)))<<std::endl;
  	         for(int b = 1; b <= 2*N-1; b+=2)
       {

  	     auto bb = AutoMPO(sites);
  
  	     bb += 1,"N",b, "B",b ;
  	     bb += 1,"N",b, "Bdag",b ;
  		      auto BB = IQMPO(bb);
  		      auto en3 = overlap(psi,BB,psi);
        printfln("%d %.12f",b,en3);
  		      auto gg = AutoMPO(sites);
  
  		        gg += 1,"Nph",b ;
  		      auto GG = IQMPO(gg);
  		      auto en2 = overlap(psi,GG,psi);
  		         printfln("%d %.12f",b,en2);
 			 auto gg2 = AutoMPO(sites);
  
  		        gg2 += 1,"N",b ;
  		      auto GG2 = IQMPO(gg2);
  		      auto en22 = overlap(psi,GG2,psi);
  		         printfln("%d %.12f",b,en22);
		        

   


  }
return 0;
}
//1.24329743531951675273
// 1.24329743531951675273
//Energy/N 0.0100 1.47754105292885817846

//Nph/N 0.0100 1.48418008688657598881
