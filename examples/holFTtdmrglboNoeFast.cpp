#include "itensor/all.h"
#include"holstein.hpp"
#include "makebasis.hpp"
#include"files.hpp"
#include"timeevclass.hpp"
#include"maketimeevop.hpp"
#include"../src/lboclass.hpp"
#include <boost/program_options.hpp>
using namespace itensor;
struct dbl_cmp {
    dbl_cmp(double v, double d) : val(v), delta(d) { }
    inline bool operator()(const double &x) const {
        return abs(x-val) < delta;
    }
  private:
    double val, delta;
};
int
main(int argc, char *argv[])
    {
      using boost::program_options::value;
  double cutoff{};
  double lbocutoff{};
  double phcutoff{};
  int M{};
  int Md{};
  int Mph{};
  int lboMd{};
  int L{};

  double t0{};
  double omega{};
  double gamma{};
  double dt{};
  double tot{};

  double T{};
  bool saveAll{};
  std::string scutoff{};
   std::string sphcutoff{};
  std::string slbocutoff{};
  std::string sM{};
  std::string sMph{};
  std::string sMd{};
  std::string slboMd{};
  std::string sL{};
  std::string st0{};
  std::string somega{};
  std::string sgamma{};
  std::string sdt{};
  std::string stot{};

  std::string sT{};
  std::string filename="NoeFTtdmrglbo";

  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("L", boost::program_options::value(&L)->default_value(4), "L")
      ("M", boost::program_options::value(&M)->default_value(4), "M")
      ("Mph", boost::program_options::value(&Mph), "Mph")
      ("Md", boost::program_options::value(&Md)->default_value(2000), "Md")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(100), "lboM")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("T", boost::program_options::value(&T)->default_value(0.1), "T")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-9), "lbocut")
      ("phcut", boost::program_options::value(&phcutoff)->default_value(1E-9), "phcut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-9), "cut")
      ("SA", boost::program_options::value(&saveAll)->default_value(0), "SA");

    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';
      	sL="L"+std::to_string(vm["L"].as<int>());
	filename+=sL;
      }
   
      	     if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
	     	     if (vm.count("Mph"))
      {      std::cout << "Mph: " << vm["Mph"].as<int>() << '\n';
      	sMph="Mph"+std::to_string(vm["Mph"].as<int>());
      	filename+=sMph;
      }
	           	     if (vm.count("Md"))
      {
	
	std::cout << "Md: " << vm["Md"].as<int>() << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
      	filename+=sMd;
      }
			           	     if (vm.count("M"))
      {      std::cout << "lboMd: " << vm["lboMd"].as<int>() << '\n';
      	slboMd="lboMd"+std::to_string(vm["lboMd"].as<int>());
      	filename+=slboMd;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      }
	 if (vm.count("T"))
      {      std::cout << "T: " << vm["T"].as<double>() << '\n';
      	sT="T"+std::to_string(vm["T"].as<double>()).substr(0, 4);
      	filename+=sT;
      }
	 if (vm.count("gam"))
      {      std::cout << "gamma: " << vm["gam"].as<double>() << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 3);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 3);
      		filename+=somega;
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << vm["tot"].as<double>() << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 3);
	//      		filename+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
		 if (vm.count("lbocut"))
		   {
		std::cout << "lbocutoff: " << vm["lbocut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["lbocut"].as<double>();
	 slbocutoff="lbocut"+ss.str();
      	filename+=slbocutoff;
      }
		 if (vm.count("phcut"))
		   {
		std::cout << "phcutoff: " << vm["phcut"].as<double>() << '\n';
	 std::stringstream ss;
	 ss<<vm["phcut"].as<double>();
	 sphcutoff="phcut"+ss.str();
      	filename+=sphcutoff;
      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  filename+=".bin";
      // GS SEARCH
     auto sites2 = Holstein(L,{"ConserveNf=",true,
                             "ConserveNb=",false,
				"MaxOcc",M});
          auto am2=makeHolstHam(sites2, t0, gamma, omega);
     auto H2 = toMPO(am2);


    auto sweeps = Sweeps(100);
    //very important to use noise for this model
    sweeps.noise() = 1E-6,1E-6,1E-8, 1E-10,  1E-12;
    sweeps.maxdim() = 10,20,100,800,800;
    sweeps.cutoff() = 1E-14;

    auto state2 = InitState(sites2);
    state2.set(1,"Occ");
     // state.set(3,"Occ");
     // state.set(5,"Occ");
    auto psi02 = MPS(state2);
     psi02.position(1);
    psi02.normalize();
    // auto KK=sites.op("F", 1);
    // print2IndT(KK);
    //      Print(psi0);
    //auto [energy2,psi2] = dmrg(H2,psi02,sweeps,{"Quiet=",true});
       // END GS SEARCH
  double beta=1./T;
  int N=2*L;
   std::vector<int> v(N, M);
   std::vector<double> Tsstore={0.1, 0.4};
				//{ 0.2, 0.3, 0.5, 0.7};
  itensor::Args argsState={"ConserveNf=",true,
                             "ConserveNb=",false,
			   "DiffMaxOcc=",true, "MaxOccVec=", v};
  auto argsMPS = Args("Cutoff=",cutoff,"MaxDimLBO",lboMd, "MaxPh", Mph,"CutoffLBO",lbocutoff  , "MaxDim=",Md, "CutPH", phcutoff,"DoNormalize",true);

     auto sites = Holstein(N, argsState);
   auto psi=makeBasisNo_e2(sites, M);

 auto ampo=makePureHolstHam(sites, t0, gamma, omega);

 auto H=toMPO(ampo);
 AutoMPO Nel(sites);
  AutoMPO Nph(sites);
      AutoMPO Ek(sites);
    AutoMPO x(sites);
    for(int b = 1; b <=N ; b+=2)
      {
	std::cout<<"  b "<< b << std::endl;
Nel += 1,"N",b ;
  x += 1,"NBdag",b ;
  x += 1,"NB",b ;
 Nph += 1,"Nph",b;


      }
        for(int b = 1; b <N-1 ; b+=2)
      {

	Ek += -t0,"Cdag",b, "C", b+2 ;
	Ek += -t0,"Cdag",b+2, "C", b ;

}

     auto NEL = toMPO(Nel);
     auto NPH = toMPO(Nph);
     auto X = toMPO(x);
     auto EK = toMPO(Ek);
   auto nt = int(beta/dt+(1e-9*(beta/dt)));


   std::vector<double> nphvec;
   std::vector<double> evec;
   std::vector<double> xvec;
   std::vector<double> ekvec;
   std::vector<double> tempvec;
    std::vector<double> lboavvec;
    std::vector<double> mxlbo;
    std::vector<double> mxmd;
     using Gate = BondGate;

    auto args = Args("Cutoff=",cutoff,"Maxm=",Md, "DoNormalize",true);
    auto gates = std::vector<Gate>();
    std::map<std::string, double> param;
 param.insert({"t0",t0});
 param.insert({"omega",omega});
 param.insert({"gamma",gamma});
 param.insert({"C",0});
    IHG GM(sites, gates, 0.5*dt, param);
    GM.makeFTGates();
       std::chrono::duration<double> elapsed_seconds;
 std::chrono::time_point<std::chrono::system_clock> start, end;
 std::cout<< "Start "<< std::endl;
     TimeEvolveLbo< IHG, decltype(psi), true> C(GM,  psi, length(psi),   argsMPS, argsState);
     std::cout<< "FAST s2Start "<< std::endl;
     //   Print(psi);
     psi.position(1);
     auto en=innerC(psi, H, psi)/innerC(psi, psi);
    std::cout<< "en/L "<< en/L <<std::endl;
      C.makeLbo();
   double beta2=0;
   MPS psir2;
   std::cout<< "here "<< std::endl;
 
    for(int n=1; n<=nt; ++n)
      {

        start = std::chrono::system_clock::now();
 	std::cout<< "st "<< std::endl;
 	C.lbotDmrgStep();
 	  end = std::chrono::system_clock::now(); 
 	 std::chrono::duration<double> elapsed_seconds = end-start;
 	  std::cout <<  "elapsed time: " << elapsed_seconds.count() << "s\n";
 	beta2+=dt;
	
        psir2=C.getBareMps();
       psir2.position(1);
       psir2.normalize();
       auto en=innerC(psir2, H, psir2);
       auto nph=innerC(psir2, NPH, psir2);
       auto ek=innerC(psir2, EK, psir2);
       auto smx=innerC(psir2, X, psir2);
       	  nphvec.push_back(real(nph));
       	  evec.push_back(real(en));
       	  xvec.push_back(real(smx));
       	  ekvec.push_back(real(ek));
     	  tempvec.push_back(1./beta2);
     	  mxlbo.push_back(static_cast<double>(C.maxLboDim()));
 	  mxmd.push_back(static_cast<double>(maxLinkDim(psir2)));
     	  lboavvec.push_back(C.avLboDim());

       printfln("\n Energy/L %.4f %.20f",1./beta2, en);
             printfln("\n Nph/L %.4f %.20f", n*dt, nph);
       	     std::cout<< "com "<< real(en) << "  "<< real(ek)+real(smx)*gamma+real(nph) << " av  lbo "<<C.avLboDim() << "  lbo mx "<<C.maxLboDim()<<"max BD "<< maxLinkDim(psir2)  <<std::endl;
    
 	     if(saveAll &&  (std::find_if(Tsstore.begin(), Tsstore.end(), dbl_cmp((1./beta2), 1E-8))!=Tsstore.end()))
 		    {

 std::string SC="aT"+std::to_string(1./beta2).substr(0,4);
 		      itensor::writeToFile("MPSFast"+SC+filename,psir2);
 		      itensor::writeToFile("sitesetFast"+SC+filename,sites);
 		    }       
	 
     }

  // //
        psir2=C.getBareMps();
       psir2.position(1);
       Print(innerC(psir2, psir2));
   for(int b = 1; b <= N; b+=2)
        {

  	     auto bb = AutoMPO(sites);
  
  	     bb += 1,"N",b, "B",b ;
  	     bb += 1,"N",b, "Bdag",b ;
  		      auto BB = toMPO(bb);
  		      auto en3 = innerC(psir2,BB,psir2)/innerC(psir2, psir2);
        printfln("X %d %.12f",b,en3);
  		      auto gg = AutoMPO(sites);
  
  		        gg += 1,"Nph",b ;
  		      auto GG = toMPO(gg);
  		      auto en2 = innerC(psir2,GG,psir2)/innerC(psir2, psir2);
  		         printfln("Nph %d %.12f",b,en2);
  			 auto gg2 = AutoMPO(sites);
  
  		        gg2 += 1,"N",b ;
  		      auto GG2 = toMPO(gg2);
  		      auto en22 = innerC(psir2,GG2,psir2)/innerC(psir2, psir2);
  		         printfln("%d %.12f",b,en22);
		        

   


   }
		 
  		 Many_Body::bin_write("tempFast"+filename, tempvec);
  		 Many_Body::bin_write("nXFast"+filename, xvec);
  		 Many_Body::bin_write("EKFast"+filename, ekvec);
  		 Many_Body::bin_write("NphFast"+filename, nphvec);
  		 Many_Body::bin_write("EFast"+filename, evec);
		 Many_Body::bin_write("mxLBOFast"+filename, mxlbo);
		 Many_Body::bin_write("avLBOFast"+filename, lboavvec);
		  Many_Body::bin_write("mxBDFast"+filename, mxmd);
		
		 
    return 0;
    
    }

