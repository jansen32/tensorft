
#include "itensor/all.h"
#include"files.hpp"
#include"holstein.hpp"
#include"../src/lboclass.hpp"
#include"../src/maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include <iomanip>
#include<utility>
#include <boost/program_options.hpp>
#include"../src/GF.hpp"
using namespace std;
using namespace itensor;

int main(int argc, char *argv[])
{
  
  double cutoff{};
  double lbocutoff{};
  double phcutoff{};
  int M{};
  int j{};
  int Md{};
  int Mph{};
  int lboMd{};
  int L{};
std::string mpsName{};
 std::string siteSetName{};
  double t0{};
  double omega{};
  double gamma{};
  double dt{};
  double tot{};

  std::string scutoff{};
   std::string sphcutoff{};
  std::string slbocutoff{};
  std::string sM{};
 std::string sj{};
  std::string sMph{};
  std::string sMd{};
  std::string slboMd{};
  std::string sL{};
  std::string st0{};
  std::string somega{};
  std::string sgamma{};
  std::string sdt{};
  std::string stot{};


  std::string filename="";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam");

    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      }
	 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 3);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 3);
      		filename+=somega;
      }
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }

   auto sites = readFromFile<Holstein>(siteSetName);
    auto psi = readFromFile<MPS>(mpsName);
    //print(psi);
   using Gate = BondGate;
    auto gates = vector<Gate>();


	 auto args = itensor::Args("Cutoff=",1e-13,"MaxDim=",3000, "Normalize", true);
 auto args2 = itensor::Args("Cutoff=",1e-13,"MaxDim=",3000);
 auto H1=itensor::makeFTHQ(sites,t0, gamma, omega);
  auto psiOne=applyMPO(H1,psi, args2);
//auto H1sqrd=itensor::multSiteOps(H2, H2);
 auto E0=itensor::innerC(psi, H1,psi);
 std::cout<< "energy "<< E0<<std::endl;
 auto E0Sqrd=itensor::innerC(psiOne,psiOne);
 std::cout<< " djei "<< E0Sqrd<<std::endl;
 	  auto VAL=E0*E0;
 std::cout<< " djei 2 2 "<< VAL<<std::endl;
 auto VARIANCE =  std::abs((E0Sqrd-VAL )/E0Sqrd);
 		    std::cout<< "THE REL GS VAR WAS "<< VARIANCE<<std::endl;
 		    std::cout<< "THE GS VAR WAS "<< std::abs(E0Sqrd-VAL )<<std::endl;
 		    if(std::abs((E0Sqrd-VAL )/E0Sqrd)>1e-04){return 1;}
 		    if(std::abs(E0Sqrd-VAL)>1e-04){return 1;}


  return 0;
}
