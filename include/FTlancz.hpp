 #ifndef FTLANCZ_H
#define FTLANCZ_H
#include "itensor/all.h"
#include "tridi.hpp"
#include<vector>
#include<Eigen/Dense>
#include "tridi.hpp"
#include<vector>
#include<Eigen/Dense>
namespace  itensor{
  double eps=1E-10;
  double cutoff=1E-13;
  int maxm=1000;
template<typename OP, typename MPS>
MPS OrthoNorm(MPS& W, std::vector<MPS>& subSpace, OP& H)
{


 auto subSpace2=subSpace;

 std::for_each(subSpace2.begin(), subSpace2.end(), [W](MPS& p)
 {
   auto Value=innerC(W,    p);
       Value*=-1;
       p*=Value;
 });
 subSpace2.push_back(W);
auto Wprime=sum(subSpace2);
	//   }
  // std::cout<< "nrm wprime "<< norm(Wprime)<<std::endl;
 Wprime.position(1);
  if(norm(Wprime)<eps){std::cout<< "inv subspace "<< std::endl;}
  Wprime.normalize();
  return Wprime;
}
  template<typename OP, typename Container>
  auto ApplyAndOrthoNorm( Container& subSpace, OP& H)
    ->typename Container::value_type
  {
    auto W=subSpace[subSpace.size()-1];

    W=applyMPO(H, W, {"Maxm=",maxm,"Cutoff=",cutoff});
    W.noPrime();
    return OrthoNorm( W, subSpace,  H);
  }
  template<typename Container, typename Op >
  void updateTri(TriDiagMat<Real>& T, const Container& subSpace, const Op& H)
  {
    size_t dim=subSpace.size();
    
    T.addElement(inner(subSpace[dim-1], H, subSpace[dim-1]), inner(subSpace[dim-1],H,  subSpace[dim-2]));
    
  }
 
  template<typename  MPS, typename MPO, typename Type>
  void krylov_fttimeStep(MPS& iniState, MPS& psi, const MPO& H, Type beta)
{
  MPS v0=iniState;
  v0.normalize();
  int nos=25; // L -> 5+1 diag el, 5+1 vec, 5 of diAG EL
   std::vector<MPS> subSpace{v0};

   TriDiagMat<Real> T;
   double Norm=norm(iniState);
   T.addDiagElement(inner(v0, H, v0));
  for(int i=1; i<=nos; i++)
    {

         MPS v=ApplyAndOrthoNorm(subSpace, H);
        subSpace.push_back(v);

         updateTri(T, subSpace, H);
 
	 //	Cs.push_back(getCs(TE, dt));  
}
  //  std::cout<< "hei "<< std::endl;
  std::cout<< " nr of lvec "<< subSpace.size()<< std::endl;
          auto TE=T.make_tensor();
	auto mind=TE.inds()[1];
	auto mind2=TE.inds()[0];
	// Print(TE);
	// 	for(int j=1; j<=nos+1; j++)
	//   {
	//     	for(int i=1; i<=nos+1; i++)
	//   {
	  
	// 	std::cout<< elt(TE, j, i)<< " ";
	//   }
	  
	// 	std::cout<< std::endl;
	//   }
	auto [U,D] = diagHermitian(TE);
	//	U=dag(U);
	std::vector<MPS> evs;
	// Print(U);
	// 	for(int j=1; j<=nos+1; j++)
	//   {
	//     	for(int i=1; i<=nos+1; i++)
	//   {
	//     	    	    std::cout<< elt(U, j, i)<< " ";
	//   }
	// 	std::cout<< std::endl;
	//   }
	// 	std::cout<< std::endl;
	for(int j=1; j<=nos+1; j++)
	  {
	    //std::cout<< elt(U, 1, j)<<std::endl;
	     MPS p=subSpace[0]*elt(U, 1, j);
	for(int i=2; i<=nos+1; i++)
	  {
	    p=sum(p, elt(U, i, j)*subSpace[i-1], {"Maxm=",maxm,"Cutoff=",cutoff});
	  }
	p.normalize();
	evs.push_back(p);
	  }
	
	Cplx A={0, 0};
	Cplx Z={0, 0};
	int max{0};

	for(int k=1; k<=nos+1; k++)
	  {
	    // std::cout<< elt(D, k, k)<<std::endl;
	    int m=maxLinkDim(evs[k-1]);
	    if(m>max){max=m;}
	    A+=innerC(v0, evs[k-1])*innerC(evs[k-1], H, v0)*std::exp(-beta*elt(D, k, k));
	    Z+=std::abs(innerC(v0, evs[k-1]))*std::abs(innerC(v0, evs[k-1]))*std::exp(-beta*elt(D, k, k));
	      }
		std::cout<< A/Z<<std::endl;
		std::cout<< "max bdim was "<< max<<std::endl;
		MPS targetState=innerC(v0, evs[0])*std::exp(-beta*0.5*elt(D, 1, 1))*evs[0];
		for(int i=1; i<=nos; i++)
		  {
		    //    std::cout<< innerC(v0, evs[i])<< std::endl;
		    		    targetState=sum(targetState, innerC(v0, evs[i])*std::exp(-0.5*beta*elt(D, i+1, i+1))*evs[i], {"Normalize", false, "Maxm=",maxm,"Cutoff=",cutoff});
		  }
		//		MPS targetState=innerC(v0, evs[nos])*std::exp(-beta*0.5*elt(D, nos+1, nos+1))*evs[nos];
	// 	       {
     // 			 int i=	2;
     // targetState.position(i);
     // targetState.normalize();
     // auto phi=targetState.A(i);
     //  Print(phi);
     // auto phidag=dag(prime(phi, "Site"));
     // auto rho=phi*phidag;
     // auto tags = TagSet("Site, OM");
     // int n = i;
     //              Print(rho);
     // 		 tags.addTags("n="+str(n));
     // 		 auto [U, D] =diagPosSemiDef(rho,{"ShowEigs", false});
	

     //   }
	 // auto NewM=expHermitian(TE, dt);
  	 // auto NEUET=ITensor(mind);
	 // NEUET.set(mind(1), 1);
	 // //std::cout<< NEUET.real(mind(2))<< std::endl;
	 // auto MATT=NewM*NEUET;
	 // v0*=norm(iniState);
	 // iniState=subSpace[0]*eltC(MATT, mind2(1));
	 // psi=iniState;
	 // //	   std::cout<< "hei 2"<< std::endl;

	 //   for(int i=1; i<subSpace.size()-1; i++)
	 //   {
	 //     iniState = sum(iniState, Norm*eltC(MATT, mind2(i+1))*subSpace[i], {"Maxm=",maxm,"Cutoff=",cutoff});
	 //     psi = sum(psi, Norm*subSpace[i], {"Maxm=",maxm,"Cutoff=",cutoff});	     
	 //   }
  
}
  }

#endif /* FTLANCZ_H */
