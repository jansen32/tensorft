#ifndef BIGSITEALG_H
#define BIGSITEALG_H
#include"holstein.hpp"
#include "itensor/all.h"
#include "makebasis.hpp"
#include"timeEv.hpp"
#include <map>
#include"extra.hpp"
 namespace itensor{
   void print2IndT(ITensor& U)
   {
 for(int v=1; v< U.inds()[0].dim()+1; v++)
      		    {
      		      for(int w=1; w< U.inds()[1].dim()+1; w++)
      		        {

			  std::cout<<eltC(U, U.inds()[0](v), U.inds()[1](w))<< "  ";
      		        }
		      std::cout<< std::endl;
      		    }     
   }
 void seemat(ITensor Tens)
{
  auto oldInd=Tens.inds()[0];
  auto valInd=Tens.inds()[1];
  auto blocks = doTask(GetBlocks<Real>{Tens.inds(), oldInd, valInd}, Tens.store()); //
  auto Nblock=blocks.size();
  long n=0;
  //      std::cout<< "start " << v <<std::endl;
  for(auto b : range(Nblock))
    {
      
      //std::cout<< v.size()<<std::endl;
      
      auto& B=blocks[b];
      auto M=blocks[b].M;
      // auto M2=blocks2[b].M;
      auto rm=nrows(M);
      auto cm=ncols(M);
      // auto rm2=newInd[b].m();
      //auto cm2=valInd[b].m();
      
      
      for(auto i=0; i<rm; i++)
	{
	  for(auto j=0; j<cm; j++)
	    {
	      //  std::cout<< " b  "<< b<< std::endl;
	      //td::cout<< UU(i, j)<< "change to "<< M(i, j)<<std::endl;
	      std::cout<< M(i, j) << "  ";
	      
	      
	    }
	  std::cout<<std::endl;
	}
    }
}

template<typename T=Cplx>
   ITensor app(ITensor Uold, Index newIn)
   {

     auto oldIn=Uold.inds()[0];
     auto valIn=Uold.inds()[1];
     auto Uis = IndexSet(newIn,valIn);
     auto blocks = doTask(GetBlocks<T>{Uold.inds(),oldIn,valIn},Uold.store());
     auto Nblock = blocks.size();
     auto Ustore = QDense<T>(Uis,QN());
     size_t totalUsize = 0;
               for(auto b : range(Nblock))
            {

	      totalUsize += newIn.dim()*valIn.dim();
            }
	       std::cout<< "tot U size "<< totalUsize<<std::endl;
	       auto Udata = std::vector<T>(totalUsize);
     long n = 0;
     auto Umats = std::vector<MatRef<T>>(Nblock);
        for(auto b : range(Nblock))
            {

auto& B=blocks[b];
      auto M=blocks[b].M;
      auto rm=nrows(M);
      auto cm=ncols(M);
     
      auto& UU = Umats.at(b);
      UU = makeMatRef(Udata.data(),newIn.dim()*valIn.dim(),newIn.dim(),valIn.dim());
      std::cout<< " size  "<< UU.size()<<std::endl;
      for(auto i=0; i<std::min(int(rm), int(newIn.dim())); i++)
	{
	  for(auto j=0; j<cm; j++)
	    {
	      //  std::cout<< " b  "<< b<< std::endl;
	      //	      std::cout<< UU(i, j)<< "change to "<< M(i, j)<<std::endl;
	          UU(i, j)= M(i, j);

	      
	      
		      }
	  //	  std::cout<<std::endl;
	}
      //std::cout<< "end "<<std::endl;
	      auto mm=ncols(UU);
	      //	      Print(B.i1);

	       auto uind = stdx::make_array(B.i1,n);
	       auto pU = getBlock2<decltype(Ustore), decltype(uind)>(Ustore,Uis,uind);
              assert(pU.data() != nullptr);
              assert(ai.blocksize0(B.i1) == long(nrows(UU)));
              auto Uref = makeMatRef(pU,nrows(UU),mm);
              Uref &= UU;

 ++n;
            }
	
        auto U = ITensor(Uis,move(Ustore));
	return U;
        
   }

class bigSiteAlg
{
public:
  
  bigSiteAlg(size_t M, size_t m, size_t L):
    L_{L},  M_(M), m_{m}, N_{4*L},
    currentBigSite_(2), ms_(N_, m_)
  {
    for(int i=2; i<=N_; i+=4 ){

      OM_.insert({i, ITensor()});
    }
    ms_[currentBigSite_-1]=M_;
    ms_[currentBigSite_-1+2]=M_;
    
    std::cout<< "const "<< std::endl;
  }
    void run();
     void runFirst();
  // actualy physical sites
    size_t L_;
    // The maximum phonon number on the big site 
  size_t M_{};
  // the local maximal phonon number
  size_t m_{};
  // the current big Site
  
  // length of complete lattice inc auciller
  size_t N_;
  size_t currentBigSite_{};
  Holstein sites_;
  // all small m's stored in one vector
  std::vector<int> ms_;
  // the transformation matricies into OM
  std::map<int,ITensor> OM_;
  double beta_{2.};
  double tau_{0.01};
  ITensor TR;
  int tpd;

};
   void  bigSiteAlg::runFirst(){
   double cutoff=1E-15;
  double omega=1;
  double t0=1;
  double gamma=1.;

   sites_ = Holstein(N_,{"ConserveNf=",true,
                              "ConserveNb=",false,
       "DiffMaxOcc=",true, "MaxOccVec=", ms_});
   auto ampo=makePureHolstHam(sites_, t0, gamma, omega);
        auto psi=makeBasis(sites_, ms_);
	Print(sites_);
	//  Print(psi);
   auto nt = int(beta_/tau_+(1e-9*(beta_/tau_)));
   MPO expHa,expHb;
   auto taua = tau_/4.*(1.+1._i);
   auto taub = tau_/4.*(1.-1._i);
    expHa = toExpH(ampo,taua);
   expHb = toExpH(ampo,taub);
   std::cout<< "start "<< std::endl;
     for(int n = 1; n <= nt; ++n)
       {
	 	 imTimeEvExactStep(psi, ampo, expHa, expHb);
       }
     //     for(int i=2; i<=N_; i+=4)
       {
	 int i=	currentBigSite_; 
     psi.position(i);
     auto phi=psi.A(i);
     //  Print(phi);
     auto phidag=dag(prime(phi, "Site"));
     auto rho=phi*phidag;
     auto tags = TagSet("Site, OM");
     int n = i;
     //         Print(rho);
		 tags.addTags("n="+str(n));
		 auto [U, D] =diagPosSemiDef(rho,{"ShowEigs", true, "Tags", tags,"Cutoff",cutoff});
		 auto x=(D.inds()[0]);
		 auto x1=x.tags();
		 auto x2=sites_(i).tags();
		   //U.inds()[0].tags();
		 x.replaceTags(x1, x2);
		 ITensor U2=U;
		 auto Unew =U;
		 //app(U2, x);
		 TR=Unew;
		 OM_[i]=Unew;
		 ms_[i-1]=M_;
		   //D.inds()[0].dim()-1;
		   ms_[i-1+2]=M_;
		   //D.inds()[0].dim()-1;
		   tpd=M_;
		   //D.inds()[0].dim()-1;

       }
       std::cout<<" kkkkk "<< std::endl;
          for(int i=2; i<=N_; i+=4)
       {
	 if(i!=currentBigSite_)
	   {
	     //     psi.position(i);
     // auto phi=psi.A(i);
     // //  Print(phi);
     // auto phidag=dag(prime(phi, "Site"));
     // auto rho=phi*phidag;
 int n = i;
	     auto tags = TagSet("Site, OM, n="+str(n));
    
     //         Print(rho);
     //     tags.addTags("n="+str(n));
      auto x=findIndex(TR, "OM");
      //      Print(x);
      x.replaceTags(TagSet("n="+str(currentBigSite_)), TagSet("n="+str(n)));
      //     Print(x);
	
     // 		 //		 auto [U, D] =diagPosSemiDef(rho,{"ShowEigs", true, "Tags", tags});
		  
     // 		  auto x1=x.tags();
      		  		   auto x2=sites_(i);
     // 		   //U.inds()[0].tags();
     // 				   Print(x);
     // 				   Print(x1);
     // 				   Print(tags);
     // 		  x.replaceTags(x1, tags);
		 // ITensor U2=U;
		 // auto Unew =app(U2, x);

		  OM_[i]=TR;
		  OM_[i].replaceInds({findIndex(OM_[i], "Boson"), findIndex(OM_[i], "OM")}, {findIndex(OM_[i], "Boson"),x});
		  Print(OM_[i]);	 
		  ms_[i-1]=tpd;
		  ms_[i-1+2]=tpd;

	   }
       }
	  //     currentBigSite_+=4;
}    
   
void  bigSiteAlg::run()  
{
  double cutoff=1E-15;
  double omega=1;
  double t0=1;
  double gamma=1.;
  // while( currentBigSite_<=N_)
  //   {
  //     std::cout<< " cur big site "<< currentBigSite_<< std::endl;
      
  //     ms_[currentBigSite_-1]=M_;
  //   ms_[currentBigSite_-1+2]=M_;

   sites_ = Holstein(N_,{"ConserveNf=",true,
                              "ConserveNb=",false,
       "DiffMaxOcc=",true, "MaxOccVec=", ms_});
   //   Print(sites_);
   std::cout << " ms " <<std::endl;

   auto ampo=makePureHolstHam(sites_, t0, gamma, omega);
   auto H=toMPO(ampo);
   auto psi=makeBasis(sites_, ms_);
  
   auto nt = int(beta_/tau_+(1e-9*(beta_/tau_)));
   MPO expHa,expHb;
   auto taua = tau_/4.*(1.+1._i);
   auto taub = tau_/4.*(1.-1._i);
    expHa = toExpH(ampo,taua);
   expHb = toExpH(ampo,taub);
  //  std::cout<< "start with big site "<<       currentBigSite_<<std::endl;
   
   for(auto it=OM_.begin(); it!=OM_.end(); ++it)
     {
    
  //      // if(it->first==currentBigSite_ or (it->first==currentBigSite_+2))
  //      // 	 {}
  //      // else{

  	    auto& T=it->second;
	      
  	    //	    Print(T);
  // 	    //	    Print(dag(sites_(it->first)));
   	    T.replaceInds({findIndex(T, "Boson"), findIndex(T, "OM")}, {dag(findIndex(psi.ref(it->first), "Boson")),findIndex(T, "OM")});
  // 	    //	      Print(findIndex( psi.ref(it->first),"Site"));
	    //  	       Print(T);
  	      psi.ref(it->first)*=it->second;
  	      expHb.ref(it->first)*=dag(it->second);
  	      expHa.ref(it->first)*=dag(it->second);
  	      expHb.ref(it->first)*=prime(it->second);
  	      expHa.ref(it->first)*=prime(it->second);
  	      H.ref(it->first)*=dag(it->second);
  	      H.ref(it->first)*=prime(it->second);
  // 	      //}
       
      }

     for(int n = 1; n <= nt; ++n)
       {
  	 imTimeEvExactStep(psi, ampo, expHa, expHb);
       }
      std::cout<< "finished "<< std::endl;
  //     psi.normalize();
  // 	  auto en = innerC(psi,H,psi);
  // 	 printfln("\nEnergy/N %.4f %.20f",beta_, en/L_);
  //         for(int i=2; i<=N_; i+=4)
  //      {
	 
  //    psi.position(i);
  //    auto phi=psi.A(i);

  //    auto phidag=dag(prime(phi, "Site"));
  //    auto rho=phi*phidag;
  //    auto tags = TagSet("Site, OM");
  //    int n = i;
  //    std::cout<< "  i!!!!!!!!!!!!!!!! "<< i << std::endl;
  // 		 tags.addTags("n="+str(n));
  // 		 auto [U, D] =diagPosSemiDef(rho,{"ShowEigs", true, "Cutoff" , cutoff,"Tags", tags});

  // 		 auto x=(D.inds()[0]);
  // 		 auto x1=x.tags();
  // 		 auto x2=sites_(i).tags();
  // 		 x.replaceTags(x1, x2);
  // 		 ITensor U2=U;
  // 		 auto Unew =app(U2, x);

  // 		 OM_[i]=Unew;
  // 		 ms_[i-1]=D.inds()[0].dim()-1;
  // 		 ms_[i-1+2]=D.inds()[0].dim()-1;

  //      }
   	  psi.normalize();
  // 	  std::cout<< "finished "<< std::endl;
	 Print(psi);
	  auto  en = innerC(psi,H,psi);
  	 printfln("\nEnergy/N %.4f %.20f",beta_, en/L_);
  //
  //       currentBigSite_+=4;
  //}
  // calculate H
  
}

 }
#endif /* BIGSITEALG_H */
