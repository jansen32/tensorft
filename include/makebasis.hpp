
#ifndef MAKEBASIS_H
#define MAKEBASIS_H
#include "itensor/all.h"
#include <string>
#include"extra.hpp"
namespace itensor{
// template<typename T>
// void oper(AutoMPO& M, T first)
// {
//    M+=first;
   
// }
  // so far only made to normalize the boson creation operator
  template<typename Sites, typename MPS, typename Index>
  void  make_loc(const Sites& sites, MPS& psi, Index& I, int n, int offset=0)
    {
      auto s1 = sites(n);
    	 auto J = Index(QN(qn(s1, 1)),1, "U,Link");
         auto s2 =(sites(n+1));
         auto wf =ITensor( s1,s2,  J, dag(I));
	 auto d=blocksize(s1, 1);
    	for(int i=1; i<=d; i++)
    	  {
	    wf.set(s1(i+offset),s2(i+offset),J(1), dag(I)(1),  1);
      }
	psi.Aref(n) = ITensor(s1, dag(I));
    	psi.Aref(n+1) = ITensor(s2, J);
	ITensor D;
    	svd(wf,psi.Aref(n),D,psi.Aref(n+1));
    	psi.Aref(n) *= D;
    	 psi.Aref(n+1)=noPrime(psi.Aref(n+1));
	 I=J;
    }
    template<typename Sites, typename MPS, typename Index>
    void  make_loc_last(const Sites& sites, MPS& psi, Index& I,int offset=0)
    {
      	 auto N=length(sites);
     	  auto s1 = sites(N-1);
         auto s2 =(sites(N));
	 auto wf = ITensor( s1,s2,dag(I));
auto d=blocksize(s1, 1);

	
    	for(int i=1; i<=d; i++)
    	  {
    	    wf.set(s1(i+offset),s2(i+offset), dag(I)(1), 1);
    	  }
    	psi.Aref(N-1) = ITensor(s1, dag(I));
    	psi.Aref(N) = ITensor(s2);
		ITensor D;
    	svd(wf,psi.Aref(N-1),D,psi.Aref(N));
    	psi.Aref(N-1) *= D;
    	 psi.Aref(N)=noPrime(psi.Aref(N));
    }
   template<typename Sites, typename MPS, typename Index>
   void  make_loc_first(const Sites& sites, MPS& psi, Index& I, int offset=0)
    {
      	 auto s1 = sites(1);
     auto s2 = (sites(2));
     auto wf = ITensor(s1,s2, I);
	auto d=blocksize(s1, 1);
	
	for(int i=1; i<=d; i++)
	  {
	     wf.set(s1(i+offset),s2(i+ offset),I(1), 1);


	  }
         ITensor D;

 
    	psi.Aref(1) = ITensor(s1);
    	psi.Aref(2) = ITensor(s2, I);
    	svd(wf,psi.Aref(1),D,psi.Aref(2));
    	psi.Aref(1) *= D;

   
     	 psi.Aref(2)=noPrime(psi.Aref(2));
    }
  void applyNtime(MPO& Op, MPS& psi, int n, Args const& args = Args::global())
  {

    for(int i=0; i<n; i++)
      {
	psi=applyMPO(Op, psi, args);

	//	psi*=1./std::sqrt(i+1);
	psi.noPrime();
      }
    
  }
template<typename Sites>
auto  makeBasis(const Sites& sites, int M)
->std::enable_if_t<!is_Mixed<Sites>::value, MPS>  {
  
  int N=length(sites);
  auto state = InitState(sites);
  state.setAll("Emp");
 state.set(1, "Occ");
 state.set(2, "Occ");
  auto psi = MPS(state);
    // psi.position(1);
    // psi.normalize();
  for(int b = 3; b <= N; b+=2)
        {


  auto state2 = InitState(sites);
   state2.setAll("Emp");
   state2.set(b, "Occ");
   state2.set(b+1, "Occ");
  auto psi2=MPS(state2);
      psi.plusEq(psi2,{"MaxDim",500,"Cutoff",1E-15, "Normalize", false});
	}	  
 	  	           for(int b = 1; b <= N; b+=2)
        {
        
        	 std::vector<MPS> psivec;
		 for(int j=0; j<=M; j++)
         {

	   	  auto psi2=psi;
        	  
         	  auto c1 = AutoMPO(sites);
         	  auto c2 = AutoMPO(sites);
		  c1 += 1.,"Bdagnorm",b;
		  c2 += 1.,"Bdagnorm",b+1;
	  
		  
        auto C1 = toMPO(c1);
        auto C2 = toMPO(c2);
        applyNtime(C1, psi2, j, {"Normalize", false});
        applyNtime(C2, psi2, (j),  {"Normalize", false});
psivec.push_back(psi2);
	 }
psi=sum(psivec, {"Normalize", false});
	    }
	 return psi;
  }

template<typename Sites>
auto  makeBasis(const Sites& sites, std::vector<int> vecM)
  ->std::enable_if_t<!is_Mixed<Sites>::value, MPS>   
{
     
  int N=length(sites);
  auto state = InitState(sites);
  state.setAll("Emp");
 state.set(1, "Occ");
 state.set(2, "Occ");
  auto psi = MPS(state);
    // psi.position(1);
    // psi.normalize();
  for(int b = 3; b <= N; b+=2)
        {


  auto state2 = InitState(sites);
   state2.setAll("Emp");
   state2.set(b, "Occ");
   state2.set(b+1, "Occ");
  auto psi2=MPS(state2);
      psi.plusEq(psi2,{"MaxDim",500,"Cutoff",1E-15, "Normalize", false});
	}	  
 	  	           for(int b = 1; b <= N; b+=2)
        {
        	  auto psi2=psi;
        	 std::vector<MPS> psivec;
        	 	     	 for(int j=0; j<=vecM[b-1]; j++)
         {
	  
        	  psivec.push_back(psi);
         	  auto c1 = AutoMPO(sites);
         	  auto c2 = AutoMPO(sites);
		  c1 += 1.,"Bdagnorm",b;
		  c2 += 1.,"Bdagnorm",b+1;
	  
		  
        auto C1 = toMPO(c1);
        auto C2 = toMPO(c2);
        applyNtime(C1, psivec[j], j, {"Normalize", false});
        applyNtime(C2, psivec[j], j,  {"Normalize", false});
       }
psi=sum(psivec, {"Normalize", false});
	    }
	 return psi;
  }
template<typename Sites>
auto  makeBasisNo_e(const Sites& sites, int M)
->std::enable_if_t<!is_Mixed<Sites>::value, MPS>  {
  
  int N=length(sites);
  auto state = InitState(sites);
  state.setAll("Emp");

  auto psi = MPS(state);
    // psi.position(1);
    // psi.normalize();
  // for(int b = 3; b <= N; b+=2)
  //       {


  // auto state2 = InitState(sites);
  //  state2.setAll("Emp");

  // auto psi2=MPS(state2);
  //     psi.plusEq(psi2,{"MaxDim",500,"Cutoff",1E-15, "Normalize", false});
  // 	}	  
 	  	           for(int b = 1; b <= N; b+=2)
        {
        	  
        	 std::vector<MPS> psivec;
		 for(int j=0; j<=M; j++)
		   {
		     
		     
	   auto c1 = AutoMPO(sites);
	   c1 += 1.,"Bdagnorm",b;
	   auto C1 = toMPO(c1);
	   	  auto c2 = AutoMPO(sites);
		  c2 += 1.,"Bdagnorm",b+1;
        auto C2 = toMPO(c2);
	   auto psi2=psi;

        applyNtime(C1, psi2, j, {"Normalize", false});
        applyNtime(C2, psi2, (j),  {"Normalize", false});
	 psivec.push_back(psi2);

       }
		 std::cout<< "size of vec"<< psivec.size()<<std::endl;
psi=sum(psivec, {"Normalize", false});
	    }
	 return psi;
  }
  template<typename Sites>
auto  makeBasisNo_e2(const Sites& sites, int M)
 ->std::enable_if_t<!is_Mixed<Sites>::value, MPS>  {
    
  int N=length(sites);
     auto psi = MPS(sites);
     auto s1 = sites(1);
     auto s2 = (sites(2));
    
     auto I = Index(QN(qn(s1, 1)),1, "U,Link");
 
     make_loc_first( sites, psi,I);
    for(int n = 3; n <= N-2; n += 2)
        {

	  make_loc( sites, psi,I, n);
	 
        }
    make_loc_last( sites, psi,I);
	

  return psi;
	   
  }
  template<typename Sites>
auto  makeBasis2(const Sites& sites, int M)
 ->std::enable_if_t<!is_Mixed<Sites>::value, MPS>  {
  int N=length(sites);
  // make site 1
     auto psi = MPS(sites);
     auto s1 = sites(1);
     auto s2 = (sites(2));    
     auto I = Index(QN(qn(s1, 1)),1, "U,Link");
     auto wf = ITensor(s1,s2, I);
     auto d=blocksize(s1, 1);
     std::vector<MPS> MPSs;

     make_loc_first(sites, psi, I, d);
     	 psi.Aref(2)=noPrime(psi.Aref(2));
    for(int n = 3; n <= N-2; n += 2)
        {
	  
    	  make_loc( sites, psi,I, n);	 
	 
        }
    	 
	  make_loc_last(sites, psi, I);
	  MPSs.push_back(psi);
	 for(int j=3; j<N-2; j+=2)
	   {
	       auto I = Index(QN(qn(s1, 1)),1, "U,Link");
	     auto s1 = sites(1);    
   
     auto psi2 = MPS(sites);
     make_loc_first(sites, psi2, I);
	      for(int n=3; n<N-2; n+=2)
	    {
	     if(n!=j)
	       {
	      make_loc( sites, psi2,I, n);
	       }
	     if(j==n)
	       {
		 make_loc( sites, psi2,I, n, d);
	       }
	    }
	     make_loc_last( sites, psi2,I);
	     MPSs.push_back(psi2);
	     //psi.plusEq(psi2,{"MaxDim",500,"Cutoff",1E-15, "Normalize", false});
	     //	     psi=psi2;
	   }
	 // last site
	 I = Index(QN(qn(s1, 1)),1, "U,Link");
	 s1 = sites(1);
     auto psi3 = MPS(sites);
     make_loc_first(sites, psi3, I);
        for(int n = 3; n <= N-2; n += 2)
        {
	  
    	  make_loc( sites, psi3,I, n);	 
	 
        }
     	  
	make_loc_last( sites, psi3,I, d);
	  MPSs.push_back(psi3);
	 //	 Print(psi);
	  return sum(MPSs,{"MaxDim",500,"Cutoff",1E-15, "Normalize", false} );
	   
  }
template<typename Sites>
auto  makeBasisNo_e(const Sites& sites, std::vector<int> vecM)
  ->std::enable_if_t<!is_Mixed<Sites>::value, MPS>   
{
     
  int N=length(sites);
  auto state = InitState(sites);
  state.setAll("Emp");

  auto psi = MPS(state);
   
    // psi.position(1);
    // psi.normalize();
  // for(int b = 3; b <= N; b+=2)
  //       {


  // auto state2 = InitState(sites);
  //  state2.setAll("Emp");

  // auto psi2=MPS(state2);
  //     psi.plusEq(psi2,{"MaxDim",500,"Cutoff",1E-15, "Normalize", false});
  // 	}	  
 	  	           for(int b = 1; b <= N; b+=2)
        {
        	  
        	 std::vector<MPS> psivec;
		 for(int j=0; j<=vecM[b-1]; j++)
		   {
		     
		     
	   auto c1 = AutoMPO(sites);
	   c1 += 1.,"Bdagnorm",b;
	   auto C1 = toMPO(c1);
	   	  auto c2 = AutoMPO(sites);
		  c2 += 1.,"Bdagnorm",b+1;
        auto C2 = toMPO(c2);
	   auto psi2=psi;

        applyNtime(C1, psi2, j, {"Normalize", false});
        applyNtime(C2, psi2, (j),  {"Normalize", false});
	 psivec.push_back(psi2);

       }
		 std::cout<< "size of vec"<< psivec.size()<<std::endl;
psi=sum(psivec, {"Normalize", false});
	    }
	 return psi;
  }

}



#endif /* MAKEBASIS_H */
