 #ifndef TIMEEV_H
#define TIMEEV_H
#include "itensor/all.h"
namespace  itensor{
void
imTimeEvExact(MPS& psi, AutoMPO& ampo, MPO& O,
	      double tau , double ttotal)
{
   auto nt = int(ttotal/tau+(1e-9*(ttotal/tau)));
  auto taua = tau/4.*(1.+1._i);
  auto taub = tau/4.*(1.-1._i);
    MPO expHa,expHb;
    expHa = toExpH(ampo,taua);
   expHb = toExpH(ampo,taub);

  for(int n = 1; n <= nt; ++n)
    {
      psi = applyMPO(expHa,psi, {"Method=","DensityMatrix","Maxm=",1000,"Cutoff=",1E-13});
  	psi.noPrime();
        psi = applyMPO(expHb,psi, {"Method=","DensityMatrix","Maxm=",1000,"Cutoff=",1E-13});
  	psi.noPrime();
  	psi.normalize();
	auto en = innerC(psi,O,psi);
	printfln("\nEnergy/N %.4f %.20f",n*tau, en);
      }
       

      }
  void
  imTimeEvExactStep(MPS& psi, AutoMPO& ampo, MPO& expHa, MPO& expHb)
{





      psi = applyMPO(expHa,psi, {"Method=","DensityMatrix","Maxm=",1000,"Cutoff=",1E-13});
  	psi.noPrime();
        psi = applyMPO(expHb,psi, {"Method=","DensityMatrix","Maxm=",1000,"Cutoff=",1E-13});
  	psi.noPrime();
  	psi.normalize();



       

      }
  

}
#endif /* TIMEEV_H */
