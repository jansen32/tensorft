 #ifndef RANDOMSTATE_H
#define RANDOMSTATE_H
#include"holstein.hpp"
#include "itensor/all.h"
#include <chrono> 
#include <random>
#include"makebasis.hpp"
namespace itensor{
template<typename Sites>
MPS makeRdState(Sites& sites, unsigned M)
{

  auto ampo = AutoMPO(sites);
  //  auto psi = IQMPS(sites);
// finds the time between the system clock 
    //(present time) and clock's epoch 
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count(); 
      
    // minstd_rand0 is a standard 
    // linear_congruential_engine 
  std::minstd_rand0 generator (seed);
  std::uniform_int_distribution<> distr(1, sites.N());
  std::uniform_int_distribution<> distr1(0, 1);
  std::uniform_int_distribution<> distr2(0, M); 
    auto state = InitState(sites);
   state.setAll("Emp");
   state.set(distr(generator), "Occ");
    auto psi = MPS(state);
    for(int i=1; i<=length(sites); i++)
       {
    //int i=1;
         ITensor Bdag = sites.op("Bdag",i);
	 auto a=AutoMPO(sites);
	 a+=1,"Bdag", i;
	 //     	std::cout<< distr2(gaenerator)<< std::endl;
	 auto bd=toMPO(a);
	applyNtime(bd, psi,distr2(generator) );

	}
    // also add hopping ?
    psi.position(length(sites)/2);
    psi.normalize();
    //    Print(psi);
return psi;
}

}
#endif /* RANDOMSTATE_H */
