LIBRARY_DIR=$(ITENSOR)
include $(LIBRARY_DIR)/this_dir.mk
include $(LIBRARY_DIR)/options.mk
INC+=-I${TENSORTOOLS}/include
INC+=-I${TENSOR}/tensortimeev/src
INC+=-I$(PWD)/src/
INC+=-I${EIGEN}
INC+=-I${EINC}
INC+=-I${MANYBODY}/src
INC+=-I${TENSOR}/TDVP/
CCFLAGS=-I. $(ITENSOR_INCLUDEFLAGS) $(OPTIMIZATIONS) -std=c++17 -O2 -I/home/jansen32/usr/lib2/include -Wno-unused-variable 
CCGFLAGS=-I. $(ITENSOR_INCLUDEFLAGS) $(DEBUGFLAGS) -std=c++17 -O2
#LIBFLAGS=-L$(ITENSOR_LIBDIR) $(ITENSOR_LIBFLAGS) -Wl,--start-group /home/jansen32/usr/lib2/lib/libboost_program_options.a -Wl,--end-group 
LIBFLAGS=-L$(ITENSOR_LIBDIR) $(ITENSOR_LIBFLAGS) -lboost_program_options -lboost_filesystem 
LIBGFLAGS=-L$(ITENSOR_LIBDIR) $(ITENSOR_LIBGFLAGS)  -lboost_program_options
OMPF=-fopenmp 
BINS+=holFTtdmrg holFTtebd holFTtebdlbo holFTtdmrglbo holFTexapp
eigenstateTest: examples/eigenstateTest.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/eigenstateTest.cpp -o eigenstateTest $(LIBFLAGS)


holFTCB: examples/holFTCB.cpp $(ITENSOR_LIBS) src/makebasis.hpp
	$(CCCOM) $(CCFLAGS) $(INC) examples/holFTCB.cpp -o holFTCB $(LIBFLAGS)
holFTtdmrgCB: examples/holFTtdmrgCB.cpp $(ITENSOR_LIBS) src/makebasis.hpp
	$(CCCOM) $(CCFLAGS) $(INC) examples/holFTtdmrgCB.cpp -o holFTtdmrgCB $(LIBFLAGS)
holFTtebd: examples/holFTtebd.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $(CCFLAGS) $(INC) examples/holFTtebd.cpp -o holFTtebd $(LIBFLAGS) $(OMPF)
holFTtebdpara: examples/holFTtebdpara.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $(CCFLAGS) $(INC) examples/holFTtebdpara.cpp -o holFTtebdpara $(LIBFLAGS) $(OMPF)
holFTtebdlbo: examples/holFTtebdlbo.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $(CCFLAGS) $(INC) examples/holFTtebdlbo.cpp -o holFTtebdlbo $(LIBFLAGS)
holFTtebdlbopara: examples/holFTtebdlbopara.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $(CCFLAGS) $(INC) examples/holFTtebdlbopara.cpp -o holFTtebdlbopara $(LIBFLAGS)

holFTtdmrglbo: examples/holFTtdmrglbo.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $(CCFLAGS) $(INC) examples/holFTtdmrglbo.cpp -o holFTtdmrglbo $(LIBFLAGS)
holFTtdvp_bath: examples/holFTtdvp_bath.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $(CCFLAGS) $(INC) examples/holFTtdvp_bath.cpp -o holFTtdvp_bath $(LIBGFLAGS)
holFTtdmrglboFast: examples/holFTtdmrglboFast.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $(CCFLAGS) $(INC) examples/holFTtdmrglboFast.cpp -o holFTtdmrglboFast  $(LIBSPATH) $(LIBFLAGS)  $(ND)

holFTtdmrglboFastIter: examples/holFTtdmrglboFastIter.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $(CCFLAGS) $(INC) examples/holFTtdmrglboFastIter.cpp -o holFTtdmrglboFastIter $(LIBFLAGS)
holFTtdmrglboNoe: examples/holFTtdmrglboNoe.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $(CCFLAGS) $(INC) examples/holFTtdmrglboNoe.cpp -o holFTtdmrglboNoe $(LIBFLAGS)
holFTtdmrglboNoeFast: examples/holFTtdmrglboNoeFast.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $(CCFLAGS) $(INC) examples/holFTtdmrglboNoeFast.cpp -o holFTtdmrglboNoeFast $(LIBFLAGS) 
holFTtdmrglboNoeFast_trivial: examples/holFTtdmrglboNoeFast_trivial.cpp $(ITENSOR_LIBS) 
	$(CCCOM) $(CCFLAGS) $(INC) examples/holFTtdmrglboNoeFast_trivial.cpp -o holFTtdmrglboNoeFast_trivial $(LIBFLAGS) 
holFTtdmrg: examples/holFTtdmrg.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/holFTtdmrg.cpp -o holFTtdmrg $(LIBFLAGS) 

holFTtdmrg_exp: examples/holFTtdmrg_exp.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/holFTtdmrg_exp.cpp -o $@ $(LIBFLAGS) 

holFTexapp: examples/holFTexapp.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/holFTexapp.cpp -o holFTexapp $(LIBFLAGS) 

holFTexapp_exp: examples/holFTexapp_exp.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/holFTexapp_exp.cpp -o holFTexapp_exp $(LIBFLAGS) 

holFTtdvp: examples/holFTtdvp.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/holFTtdvp.cpp -o $@ $(LIBFLAGS) 
holFTtdvp_exp: examples/holFTtdvp_exp.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) $^ -o $@ $(LIBFLAGS)

holFTtdvp_lbo_ts: examples/holFTtdvp_lbo_ts.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) $^ -o $@ $(LIBFLAGS)
holFTtdvp_lbo_tsKYR: examples/holFTtdvp_lbo_tsKYR.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) $^ -o $@ $(LIBFLAGS)
FTlancz: examples/FTlancz.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/FTlancz.cpp -o FTlancz $(LIBFLAGS)
holGS: examples/holGS.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/holGS.cpp -o holGS $(LIBFLAGS)
holGS_lbo: examples/holGS_lbo.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) $^ -o $@ $(LIBGFLAGS)

holGS_bath: examples/holGS_bath.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/holGS_bath.cpp -o holGS_bath $(LIBFLAGS)

holGS_test: examples/holGS_test.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/holGS_test.cpp -o holGS_test $(LIBFLAGS)
freefermGS: examples/freefermGS.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/freefermGS.cpp -o freefermGS $(LIBFLAGS)
holGSdis: examples/holGSdis.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/holGSdis.cpp -o holGSdis $(LIBFLAGS)

holGSdisExp: examples/holGSdisExp.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/holGSdisExp.cpp -o holGSdisExp $(LIBFLAGS)
holsum_rule_test: examples/holsum_rule_test.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/holsum_rule_test.cpp -o holsum_rule_test $(LIBFLAGS)
hol_mom_B: examples/hol_mom_B.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/hol_mom_B.cpp -o hol_mom_B $(LIBFLAGS)
hol_mom_Ap: examples/hol_mom_Ap.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/hol_mom_Ap.cpp -o hol_mom_Ap $(LIBFLAGS)
hol_mom_A: examples/hol_mom_A.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/hol_mom_A.cpp -o hol_mom_A $(LIBFLAGS)

holsum_rule_testExp: examples/holsum_rule_testExp.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/holsum_rule_testExp.cpp -o holsum_rule_testExp $(LIBFLAGS)
holsum_rule_testX: examples/holsum_rule_testX.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/holsum_rule_testX.cpp -o holsum_rule_testX $(LIBFLAGS)
holsum_rule_testXExp: examples/holsum_rule_testXExp.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/holsum_rule_testXExp.cpp -o holsum_rule_testXExp $(LIBFLAGS)
FHGS: examples/FHGS.cpp $(ITENSOR_LIBS)
	$(CCCOM) $(CCFLAGS) $(INC) examples/FHGS.cpp -o FHGS $(LIBFLAGS)
clean:
	rm $(BINS)
